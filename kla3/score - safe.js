
var Score = Score || {}; // define namespace

// class used to calculate the final score for a layout after all analysis completed
// score categories are:
// distance, fingerUsage, sameFinger, sameHand

// DISTANCE
Score.distance = function (analysis, category) {
	var total = 0;
	for (var finger in KB.fingers) {
		if (KB.fingers.hasOwnProperty(finger) && KB.dScoring[finger]) {
			total += (analysis.distance[finger]) * (KB.dScoring[finger]);
		}
	}

	return category.weight * category.penalty * total / analysis.tLen;
};

// FINGER USAGE
Score.fingerUsage = function (analysis, category) {
	var total = 0;
	for (var finger in KB.fingers) {
		if (KB.fingers.hasOwnProperty(finger) && KB.fScoring[finger]) {
			total += (analysis.fingerUsage[finger] * KB.fScoring[finger]);
		}
	}

	return category.weight * category.penalty * total / analysis.tLen;
};

// CONSEC FINGER
Score.sameFinger = function (analysis, category) {
	var total = 0;
	for (var finger in KB.fingers) {
		if (KB.fingers.hasOwnProperty(finger) && KB.fScoring[finger]) {
			total += (analysis.consecFingerPress[finger] * KB.fScoring[finger]);
		}
	}

	return category.weight * category.penalty * total / analysis.tLen;
};

// CONSEC HAND
Score.sameHand = function (analysis, category) {
	var total = (analysis.consecHandPress.left + analysis.consecHandPress.right);

	return category.weight * category.penalty * total / analysis.tLen;
};

// WORDS
Score.words = function (analysis, category) {
//	var total = analysis.homeBlockWords.score ? (analysis.singleHandWords.score / (analysis.homeBlockWords.score)) : 0;

//	var total = (category.numWords * 0.8 + (analysis.singleHandWords.score - analysis.homeBlockWords.score));

	var total = (Math.pow(category.numWords - analysis.homeBlockWords.score, 2) + Math.pow(/*category.numWords + */analysis.singleHandWords.score, 2));

//console.log("singleHandWords: " + analysis.singleHandWords.score);
//console.log("homeBlockWords: " + analysis.homeBlockWords.score);
//console.log("total: " + total);

	return category.weight * category.penalty * total / (category.numWords * category.numWords || 1);
};
