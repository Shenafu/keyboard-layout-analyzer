/* jshint node: true, sub: true, shadow: true */
/* globals KB, angular, KLA, $ */

/*
Service for computing keyboard efficiency

It is simply a wrapper around the KLA.Analyzer singleton
*/
var appServices = appServices || angular.module('kla.services', []);

appServices.factory('analyzer', [
function () {
		return KLA.Analyzer;
}

]);
/*
Service for maintaining the keyboards
*/

var appServices = appServices || angular.module('kla.services', []);

appServices.factory('keyboards', ['$http',

function ($http) {
		var me = {},
			layouts = []
		;

		me.newLayout = function () {
			return {"keySet": KB.keySet.standard,
						"keyMap": KB.keyMap.standard.s683_225,
						"keyboard": null
			};
		};

		me.loadLayout = function (file, index) {
			var val = file.split(".");
			var name = val[0], type = val[1];
			$http.get('../layouts/' + file).then(
				function (response) {
					var res = me.parseKeySet(response.data);
					if (res.valid) {
						res.keySet.keyboardType = type;
						me.setLayout(index, {
							keySet: $.extend(true, {}, res.keySet),
							keyMap: $.extend(true, {}, me.getKeyMapFromKeyboardType(type))
						});
					} else {
						alert(res.reason);
					}
				},
				function (response) {
					// error loading layout
					alert('Unexpected Error. Layout not found; not loaded.');
				}
			);
		};

		me.registerKeyboard = function (index, elmId) {
			if (typeof layouts[index] === 'undefined') {
				throw Error("keyboards service: Invalid index");
			}

			layouts[index].keyboard = new KB.Keyboard({
				container: elmId,
				layout: layouts[index]
			});
		};

		me.forEach = function (cb) {
			var ii = 0;
			for (ii = 0; ii < layouts.length; ii++) {
				cb(layouts[ii]);
			}
		};

		me.getKeyMapFromKeyboardType = function (keyboardType) {
			if (typeof KB.keyMap[keyboardType] === 'undefined' || typeof KB.keyMap[keyboardType].s683_225 === 'undefined') {
				throw Error("Invalid keyboard type.");
			}

			return KB.keyMap[keyboardType].s683_225;
		};

		me.fixKeys = function (keys, keyMap) {
			var newKeys = [];
			var maxKeys = keyMap.numKeys;

			// reorder keys by id, not array order
			for (var i=0, L= keys.length; i < L; i++) {
				var id = keys[i].id;
				newKeys[id] = keys[i];
			}

			// fill in undefined keys
			for (var ii = 0; ii < maxKeys; ii++) {
				if (newKeys[ii] == null) {
					newKeys[ii] = {"id": ii, "finger": 1, "primary": -1, "shift": -1, "altGr": -1, "shiftAltGr": -1, "numpad": -1};
				}
			}

			return newKeys;
		};

		me.getLayouts = function () {
			return layouts;
		};

		me.getLayout = function (index) {
			if (typeof layouts[index] === 'undefined') {
				throw Error("keyboards service: Invalid index");
			}

			return layouts[index];
		};

		me.setLayout = function (index, layout) {
			if (typeof layouts[index] === 'undefined') {
				throw Error("keyboards service: Invalid index");
			}

			layout.keySet.keys = me.fixKeys(layout.keySet.keys, layout.keyMap);

			layouts[index].keySet = layout.keySet;
			layouts[index].keyMap = layout.keyMap;
			if (layouts[index].keyboard !== null) {
				layouts[index].keyboard.setLayout(layout);
			}
		};

		me.getKeySet = function (index) {
			if (typeof layouts[index] === 'undefined') {
				throw Error("keyboards service: Invalid index");
			}

			return layouts[index].keySet;
		};

		me.presetLayouts = function () {
			for (var i=0; i < DB.vars.numPresets; i++) {
				layouts[i] = me.newLayout();
				me.loadLayout(DB.layouts[i].file, i);
			}

		}();

		me.toVsv = function (index) {
			if (typeof layouts[index] === 'undefined') {
				throw Error("keyboards service: Invalid index");
			}

			return layouts[index].keyboard.toVsv();
		};

		me.setLayoutName = function (index, name) {
			if (typeof layouts[index] === 'undefined') {
				throw Error("keyboards service: Invalid index");
			}

			layouts[index].keySet.label = name;
		};
		me.getLayoutName = function (index) {
			if (typeof layouts[index] === 'undefined') {
				throw Error("keyboards service: Invalid index");
			}

			return layouts[index].keySet.label;
		};

		me.getMoreInfoUrl = function (index) {
			if (typeof layouts[index] === 'undefined') {
				throw Error("keyboards service: Invalid index");
			}

			return layouts[index].keySet.moreInfoUrl;
		};
		me.getMoreInfoText = function (index) {
			if (typeof layouts[index] === 'undefined') {
				throw Error("keyboards service: Invalid index");
			}

			return layouts[index].keySet.moreInfoText;
		};

		me.getKeyboardType = function (index) {
			if (typeof layouts[index] === 'undefined') {
				throw Error("keyboards service: Invalid index");
			}

			return layouts[index].keySet.keyboardType;
		};

		me.parseKeySetVsv = function(txt) {
			var vv = {};
			vv.label = 'Nameless Keyboard';
			vv.fingerStart = {};
			vv.keyboardType = 'standard';
			vv.author = '';
			vv.authorUrl = '';
			vv.moreInfoUrl = '';
			vv.moreInfoText = '';
			vv.keys = [];
			//alert("VSV");
			var aa = VSV.mapTo.array(txt);
			//var section = "meta";
			var key; // current key, temporary

			aa.forEach(function(row) {
					var kw = row.shift(); // remove and save "header" or "data"
					if (kw=="data") {
						kw = row.shift(); // remove and save keyword
						switch (kw) {	// perform action based on keyword
							case 'label':
							case 'keyboardType':
							case 'author':
							case 'authorUrl':
							case 'moreInfoUrl':
							case 'moreInfoText':
								vv[kw] = typeof row[0] === 'string' ? row[0].trim() : '';
								break;
							case 'fingerStart':
								try {
									for (var i=0; i<11; i++) {
										var k = (i+1);
										vv.fingerStart[k] = parseInt(row[i]);
									}
									vv.fingerStart['false'] = parseInt(row[11]);
								} catch (err) {
									// index out of range
									return {
										valid: false,
										reason: "index out of range. must be 12 finger values."
									};
								}
								break;
							case 'key':
								key = {"id": parseInt(row[0]), "finger": 1, "primary": -1, "shift": -1, "altGr": -1, "shiftAltGr": -1, "numpad": -1};
								vv.keys.push(key);
								break;
							case 'finger':
							case 'primary':
							case 'shift':
							case 'altGr':
							case 'shiftAltGr':
							case 'numpad':
								try {
									if (row[0]) {
										key[kw] = parseInt(row[0]);
									}
								} catch (err) {
									return {
										valid: false,
										reason: "Define 'key' first."
									};
								}
								break;
						}
					}

				}
			);

			// fill in missing keys
			//vv.keys = me.fixKeys(vv.keys, me.getKeyMapFromKeyboardType(vv.keyboardType));

			//console.log(vv);

			return vv;
		}

		me.parseKeySet = function (txt) {
			var nn = {};
			var isJson = false, isVsv = false;
			if (typeof txt === 'string' && txt.match(VSV.fileHeader)) {
				try {
					return {
						valid: true,
						keySet: me.parseKeySetVsv(txt)
					};
				} catch (er2) {
					return {
						valid: false,
						reason: "parseKeySet: Invalid VSV."
					};
				}
			} else {
				try {
					if (typeof txt === 'string') {
						nn = JSON.parse(txt);
					} else {
						nn = txt;
					}
					isJson = true;
				} catch (err) {
					return {
						valid: false,
						reason: "parseKeySet: Invalid JSON."
					};
				}
			}
			var vv = {}, // actual keyboard data to be returned
				prop;
			if (typeof nn.label === "string") {
				vv.label = nn.label;
			} else {
				return {
					valid: false,
					reason: "Label not a string."
				};
			}
			if (typeof nn.fingerStart === "object") {
				vv.fingerStart = {};
				for (prop in nn.fingerStart) {
					if (typeof nn.fingerStart[prop] === "number") {
						vv.fingerStart[prop] = nn.fingerStart[prop];
					} else {
						return {
							valid: false,
							reason: "Finger start is not a number."
						};
					}
				}
				//console.log(vv.fingerStart);
			} else {
				return {
					valid: false,
					reason: "Finger start is not a object."
				};
			}
			if (typeof nn.keyboardType === "string") {
				vv.keyboardType = nn.keyboardType;
			} else {
				return {
					valid: false,
					reason: "Keyboard type is not a string."
				};
			}
			if (typeof nn.author === "string" || typeof nn.author === 'undefined') {
				vv.author = nn.author || 'Unknown';
			} else {
				return {
					valid: false,
					reason: "Keyboard author is defined and is not a string."
				};
			}

			// deprecated, ignore
			if (typeof nn.authorUrl === "string" || typeof nn.authorUrl === 'undefined') {
				vv.authorUrl = nn.authorUrl || '';
			} else {
				return {
					valid: false,
					reason: "Keyboard authorUrl is defined and is not a string."
				};
			}


			if (typeof nn.moreInfoUrl === "string" || typeof nn.moreInfoUrl === 'undefined') {
				vv.moreInfoUrl = nn.moreInfoUrl || '';
			} else {
				return {
					valid: false,
					reason: "Keyboard moreInfoUrl is defined and is not a string."
				};
			}
			if (typeof nn.moreInfoText === "string" || typeof nn.moreInfoText === 'undefined') {
				vv.moreInfoText = nn.moreInfoText || '';
			} else {
				return {
					valid: false,
					reason: "Keyboard moreInfoText is defined and is not a string."
				};
			}

			if (typeof nn.keys === "object" && typeof nn.keys.length === "number") {
//			console.log("nn.keys.length: " + nn.keys.length);
				vv.keys = [];
				for (var ii = 0; ii < nn.keys.length; ii++) {
					var key = nn.keys[ii];
					if (typeof key === "object") {
						for (prop in key) {
							if (typeof key[prop] !== "string" && typeof key[prop] !== "number") {
								return {
									valid: false,
									reason: "Key prop is not a string or number."
								};
							}
						}
						vv.keys.push(key);
					} else {
						return {
							valid: false,
							reason: "Key item is not an object."
						};
					}
				}

				// fill in missing keys
				vv.keys = me.fixKeys(vv.keys, me.getKeyMapFromKeyboardType(vv.keyboardType));
//			console.log("vv.keys.length:" + vv.keys.length);
			} else {
				return {
					valid: false,
					reason: "Keys are not an array."
				};
			}

			// debug
//			console.log("vv.keys (below)");
//			console.log(vv.keys);

			//console.log(vv);

			return {
				valid: true,
				keySet: vv
			};
		}; // end function

		return me;
}

]);
/*
Service for storing globally available data
*/

var appServices = appServices || angular.module('kla.services', []);

appServices.factory('library', [
function () {
		var me = {},
			data = {};

		me.get = function (prop) {
			if (typeof prop === 'undefined') {
				return data;
			} else {
				return data[prop];
			}
		};

		me.set = function (prop, val) {
			data[prop] = val;
		};

		return me;
}

]);
/*
Generates and formats the results
*/

var appServices = appServices || angular.module('kla.services', []);

appServices.factory('resultsGenerator', ['$log', 'keyboards', 'analyzer', 'library',

function ($log, keyboards, analyzer, library) {
		var me = {};

		/*
		Throws an Error if it fails
		*/
		me.go = function (txt) {

			// --------------------------------------------------------------------
			// Create an analysis report on each layout

			var analysis = [];
			var kLayouts = [];
			keyboards.forEach(function (layout) {
				analysis[analysis.length] = analyzer.examine({
					text: txt,
					keyMap: layout.keyMap,
					keySet: layout.keySet
				});

				var idx = kLayouts.length;
				kLayouts[idx] = {};
				kLayouts[idx].keyMap = layout.keyMap;
				kLayouts[idx].keySet = layout.keySet;
			});

			if (analysis.length === 0) {
				throw new Error('You must set at least 1 layout to display results.');
			}

			var errorInfo = {};
			errorInfo.count = 0;
			errorInfo.data = {};
			for (var aa = 0; aa < analysis.length; aa++) {
				if (analysis[aa].errors.length > 0) {
					errorInfo.data['Keyboard #' + (aa + 1)] = analysis[aa].errors;
					errorInfo.count++;
				}
			}
			library.set('errors', errorInfo);

			$log.info(analysis);

			// --------------------------------------------------------------------
			// Compute best layout

			var scores = analyzer.scoreLayouts(analysis, txt);
			library.set('summary', {
				bestLayout: scores.finalList[0].layoutName,
				rankedLayouts: scores.finalList,
				scoreClass: scores.ScoreClass
			});

			// --------------------------------------------------------------------
			// Prepare charts

			var displayData = {};
			displayData['All'] = [{
					label: 'Left Pinky',
					color: 'rgba(  0,255,255,0.5)',
					data: [KB.finger.LEFT_PINKY]
},
				{
					label: 'Left Ring',
					color: 'rgba(  0,  0,230,0.5)',
					data: [KB.finger.LEFT_RING]
},
				{
					label: 'Left Middle',
					color: 'rgba(136,136,255,0.5)',
					data: [KB.finger.LEFT_MIDDLE]
},
				{
					label: 'Left Index',
					color: 'rgba(255,  0,255,0.5)',
					data: [KB.finger.LEFT_INDEX]
},
				{
					label: 'Left Thumb',
					color: 'rgba(255,255,255,0.5)',
					data: [KB.finger.LEFT_THUMB]
},
				{
					label: 'Right Thumb',
					color: 'rgba(204,204,204,0.5)',
					data: [KB.finger.RIGHT_THUMB]
},
				{
					label: 'Right Index',
					color: 'rgba(255,  0,  0,0.5)',
					data: [KB.finger.RIGHT_INDEX]
},
				{
					label: 'Right Middle',
					color: 'rgba(255,136,  0,0.5)',
					data: [KB.finger.RIGHT_MIDDLE]
},
				{
					label: 'Right Ring',
					color: 'rgba(255,255,  0,0.5)',
					data: [KB.finger.RIGHT_RING]
},
				{
					label: 'Right Pinky',
					color: 'rgba(  0,255,  0,0.5)',
					data: [KB.finger.RIGHT_PINKY]
}
];
			displayData['Fingers'] = [{
					label: 'Left Pinky',
					color: 'rgba(  0,255,255,0.5)',
					data: [KB.finger.LEFT_PINKY]
},
				{
					label: 'Left Ring',
					color: 'rgba(  0,  0,230,0.5)',
					data: [KB.finger.LEFT_RING]
},
				{
					label: 'Left Middle',
					color: 'rgba(136,136,255,0.5)',
					data: [KB.finger.LEFT_MIDDLE]
},
				{
					label: 'Left Index',
					color: 'rgba(255,  0,255,0.5)',
					data: [KB.finger.LEFT_INDEX]
},
				{
					label: 'Right Index',
					color: 'rgba(255,  0,  0,0.5)',
					data: [KB.finger.RIGHT_INDEX]
},
				{
					label: 'Right Middle',
					color: 'rgba(255,136,  0,0.5)',
					data: [KB.finger.RIGHT_MIDDLE]
},
				{
					label: 'Right Ring',
					color: 'rgba(255,255,  0,0.5)',
					data: [KB.finger.RIGHT_RING]
},
				{
					label: 'Right Pinky',
					color: 'rgba(  0,255,  0,0.5)',
					data: [KB.finger.RIGHT_PINKY]
}
];
			displayData['Left Hand'] = [{
					label: 'Left Pinky',
					color: 'rgba(  0,255,255,0.5)',
					data: [KB.finger.LEFT_PINKY]
},
				{
					label: 'Left Ring',
					color: 'rgba(  0,  0,230,0.5)',
					data: [KB.finger.LEFT_RING]
},
				{
					label: 'Left Middle',
					color: 'rgba(136,136,255,0.5)',
					data: [KB.finger.LEFT_MIDDLE]
},
				{
					label: 'Left Index',
					color: 'rgba(255,  0,255,0.5)',
					data: [KB.finger.LEFT_INDEX]
},
				{
					label: 'Left Thumb',
					color: 'rgba(255,255,255,0.5)',
					data: [KB.finger.LEFT_THUMB]
}
];
			displayData['Right Hand'] = [{
					label: 'Right Thumb',
					color: 'rgba(204,204,204,0.5)',
					data: [KB.finger.RIGHT_THUMB]
},
				{
					label: 'Right Index',
					color: 'rgba(255,  0,  0,0.5)',
					data: [KB.finger.RIGHT_INDEX]
},
				{
					label: 'Right Middle',
					color: 'rgba(255,136,  0,0.5)',
					data: [KB.finger.RIGHT_MIDDLE]
},
				{
					label: 'Right Ring',
					color: 'rgba(255,255,  0,0.5)',
					data: [KB.finger.RIGHT_RING]
},
				{
					label: 'Right Pinky',
					color: 'rgba(  0,255,  0,0.5)',
					data: [KB.finger.RIGHT_PINKY]
}
];
			displayData['Left Fingers vs Right Fingers vs Thumbs'] = [{
					label: 'Left Fingers',
					color: 'rgba(  0,  0,230,0.5)',
					data: [KB.finger.LEFT_PINKY, KB.finger.LEFT_RING, KB.finger.LEFT_MIDDLE, KB.finger.LEFT_INDEX]
},
				{
					label: 'Right Fingers',
					color: 'rgba(255,  0,  0,0.5)',
					data: [KB.finger.RIGHT_INDEX, KB.finger.RIGHT_MIDDLE, KB.finger.RIGHT_RING, KB.finger.RIGHT_PINKY]
},
				{
					label: 'Thumbs',
					color: 'rgba(204,204,204,0.5)',
					data: [KB.finger.LEFT_THUMB, KB.finger.RIGHT_THUMB]
}
];
			displayData['Hand vs Hand'] = [{
					label: 'Left Hand',
					color: 'rgba(  0,  0,230,0.5)',
					data: [KB.finger.LEFT_PINKY, KB.finger.LEFT_RING, KB.finger.LEFT_MIDDLE, KB.finger.LEFT_INDEX, KB.finger.LEFT_THUMB]
},
				{
					label: 'Right Hand',
					color: 'rgba(255,  0,  0,0.5)',
					data: [KB.finger.RIGHT_INDEX, KB.finger.RIGHT_MIDDLE, KB.finger.RIGHT_RING, KB.finger.RIGHT_PINKY, KB.finger.RIGHT_THUMB]
}
];
			displayData['Pinky vs Pinky'] = [{
					label: 'Left Pinky',
					color: 'rgba(  0,  0,230,0.5)',
					data: [KB.finger.LEFT_PINKY]
},
				{
					label: 'Right Pinky',
					color: 'rgba(255,  0,  0,0.5)',
					data: [KB.finger.RIGHT_PINKY]
}
];
			displayData['Ring vs Ring'] = [{
					label: 'Left Ring',
					color: 'rgba(  0,  0,230,0.5)',
					data: [KB.finger.LEFT_RING]
},
				{
					label: 'Right Ring',
					color: 'rgba(255,  0,  0,0.5)',
					data: [KB.finger.RIGHT_RING]
}
];
			displayData['Middle vs Middle'] = [{
					label: 'Left Middle',
					color: 'rgba(  0,  0,230,0.5)',
					data: [KB.finger.LEFT_MIDDLE]
},
				{
					label: 'Right Middle',
					color: 'rgba(255,  0,  0,0.5)',
					data: [KB.finger.RIGHT_MIDDLE]
}
];
			displayData['Index vs Index'] = [{
					label: 'Left Index',
					color: 'rgba(  0,  0,230,0.5)',
					data: [KB.finger.LEFT_INDEX]
},
				{
					label: 'Right Index',
					color: 'rgba(255,  0,  0,0.5)',
					data: [KB.finger.RIGHT_INDEX]
}
];
			displayData['Thumb vs Thumb'] = [{
					label: 'Left Thumb',
					color: 'rgba(  0,  0,230,0.5)',
					data: [KB.finger.LEFT_THUMB]
},
				{
					label: 'Right Thumb',
					color: 'rgba(255,  0,  0,0.5)',
					data: [KB.finger.RIGHT_THUMB]
}
];

			var rowDisplayData = {};
			rowDisplayData['All'] = [{
					label: 'Number Row',
					color: 'rgba(  0,255,255,0.5)',
					data: [1]
},
				{
					label: 'Top Row',
					color: 'rgba(  0,  0,230,0.5)',
					data: [2]
},
				{
					label: 'Home Row',
					color: 'rgba(136,136,255,0.5)',
					data: [3]
},
				{
					label: 'Bottom Row',
					color: 'rgba(255,  0,255,0.5)',
					data: [4]
},
				{
					label: 'Spacebar Row',
					color: 'rgba(255,255,255,0.5)',
					data: [5]
}
];
			rowDisplayData['Number, Top, Home, Bottom'] = [{
					label: 'Number Row',
					color: 'rgba(  0,255,255,0.5)',
					data: [1]
},
				{
					label: 'Top Row',
					color: 'rgba(  0,  0,230,0.5)',
					data: [2]
},
				{
					label: 'Home Row',
					color: 'rgba(136,136,255,0.5)',
					data: [3]
},
				{
					label: 'Bottom Row',
					color: 'rgba(255,  0,255,0.5)',
					data: [4]
}
];
			rowDisplayData['Top, Home, Bottom'] = [{
					label: 'Top Row',
					color: 'rgba(  0,  0,230,0.5)',
					data: [2]
},
				{
					label: 'Home Row',
					color: 'rgba(136,136,255,0.5)',
					data: [3]
},
				{
					label: 'Bottom Row',
					color: 'rgba(255,  0,255,0.5)',
					data: [4]
}
];

			var unitConverter = function (rawVal, pixelsPerCm, unit, finger) {

				var units = {};
				units["Centimeters"] = rawVal / pixelsPerCm;
				units["Meters"] = units["Centimeters"] * 0.01;
				units["Feet"] = units["Meters"] * 3.2808399;
				units["Miles"] = units["Meters"] * 0.000621371192;
				units["Distance Penalty"] = units["Centimeters"] * (finger ? KB.dScoring[finger] : 1);
				units["Finger Penalty"] = rawVal * (finger ? KB.fScoring[finger] : 1);
				units['Key Presses'] = rawVal;
				return units[unit];
			};

			var displayFilter = function (displayType, unitType, rawSeriesData, displayData) {
				var idx, sIndex = 0,
					ii, jj, items, val,
					ppm, rawData,
					seriesData = [],
					series,
					labels = [],
					seriesLabels = [],
					allSeriesLabels = [],
					colors = [],
					seriesColors = [];

				// separate out labels and colors
				for (ii = 0; ii < displayData[displayType].length; ii++) {
					labels.push(displayData[displayType][ii].label);
					colors.push(displayData[displayType][ii].color);
				}

				for (idx = 0; idx < rawSeriesData.length; idx++) {
					allSeriesLabels.push(rawSeriesData[idx].label);
					if (!rawSeriesData[idx].visible) continue;

					ppm = analysis[idx].pixelsPerCm;
					rawData = rawSeriesData[idx].data;

					series = [];
					seriesLabels.push(rawSeriesData[idx].label);
					seriesColors.push(rawSeriesData[idx].color);

					for (ii = 0; ii < displayData[displayType].length; ii++) {
						items = displayData[displayType][ii].data;
						val = 0;
						for (jj = 0; jj < items.length; jj++) {
							val += rawData[items[jj] - 1];
						}
						series.push(val);
					}

					var total = 0;

					if (unitType === 'Percent') {
						// get total from sum of all series items
						for (var ii = 0, len = series.length; ii < len; ii++) {
							total += series[ii];
						}
						// get percentage from each series item
						for (var ii = 0, len = series.length; ii < len; ii++) {
							series[ii] = (total > 0) ? (series[ii] / total) * 100 : 0;
							series[ii] = (!isFinite(series[ii])) ? 0 : series[ii];
						}
						total = 100;
					} else {
						for (var ii = 0, len = series.length; ii < len; ii++) {
							items = displayData[displayType][ii].data;
							series[ii] = unitConverter(series[ii], ppm, unitType, (items.length == 1 ? items[0] : null));
							total += series[ii];
						}
					}

					series.total = total;
					seriesData[sIndex] = series;
					sIndex++;
				}
				seriesData.labels = labels;
				seriesData.colors = colors;
				seriesData.allSeriesLabels = allSeriesLabels;
				seriesData.seriesLabels = seriesLabels;
				seriesData.seriesColors = seriesColors;
				return seriesData;
			};


			var seriesColors = [];
			for (var i = 0; i < analysis.length; i++) {
				var r = Math.floor(Math.random() * 255);
				var g = Math.floor(Math.random() * 255);
				var b = Math.floor(Math.random() * 255);
				seriesColors[i] = 'rgb(' + r + ',' + g + ',' + b + ')';
			}

			var distSeriesData = [];
			var fuSeriesData = [];
			var rollsSeriesData = [];
			var rollsInoutData = [];
			var balanceSeriesData = [];
			var rowSeriesData = [];
			var cfuSeriesData = [];
			var cfuidSeriesData = [];
			var chuSeriesData = [];
			var chuidSeriesData = [];
			var modSeriesData = [];
			var homeKeyWordsData = [];
			var homeBlockWordsData = [];
			var singleHandWordsData = [];
			var keyData = [];
			var ii, jj;


			for (ii = 0; ii < analysis.length; ii++) {
				keyData[ii] = [];
				for (jj = 0; jj < analysis[ii].keyData.length; jj++) {
					var kData = {};
					kData.count = analysis[ii].keyData[jj].count;
					kData.cx = kLayouts[ii].keyMap[jj].cx;
					kData.cy = kLayouts[ii].keyMap[jj].cy;
					kData.primary = kLayouts[ii].keySet.keys[jj].primary;
					kData.shift = kLayouts[ii].keySet.keys[jj].shift;
					kData.altGr = kLayouts[ii].keySet.keys[jj].altGr;
					kData.shiftAltGr = kLayouts[ii].keySet.keys[jj].shiftAltGr;
					kData.numpad = kLayouts[ii].keySet.keys[jj].numpad;
					keyData[ii].push(kData);
				}
			}

			for (ii = 0; ii < analysis.length; ii++) {
				distSeriesData.push({
					label: analysis[ii].layoutName,
					color: seriesColors[ii],
					data: analysis[ii].distance.slice(1),
					visible: true
				});
				fuSeriesData.push({
					label: analysis[ii].layoutName,
					color: seriesColors[ii],
					data: analysis[ii].fingerUsage.slice(1),
					visible: true
				});
				rollsSeriesData.push({
					label: analysis[ii].layoutName,
					color: seriesColors[ii],
					data: analysis[ii].rolls.slice(1),
					visible: true
				});
				rollsInoutData.push({
					label: analysis[ii].layoutName,
					color: seriesColors[ii],
					data: analysis[ii].rollsInout,
					visible: true
				});
				balanceSeriesData.push({
					label: analysis[ii].layoutName,
					color: seriesColors[ii],
					data: analysis[ii].balance.slice(1),
					visible: true
				});
				rowSeriesData.push({
					label: analysis[ii].layoutName,
					color: seriesColors[ii],
					data: analysis[ii].rowUsage.slice(0),
					visible: true
				});
				cfuSeriesData.push({
					label: analysis[ii].layoutName,
					color: seriesColors[ii],
					data: analysis[ii].consecFingerRowJump.slice(1),
					visible: true
				});
				chuSeriesData.push({
					label: analysis[ii].layoutName,
					color: seriesColors[ii],
					data: analysis[ii].consecHandPress,
					visible: true
				});
				chuidSeriesData.push({
					label: analysis[ii].layoutName,
					color: seriesColors[ii],
					data: analysis[ii].consecHandPressIgnoreDups,
					visible: true
				});
				modSeriesData.push({
					label: analysis[ii].layoutName,
					color: seriesColors[ii],
					data: analysis[ii].modifierUse,
					visible: true
				});
				homeKeyWordsData.push({
					label: analysis[ii].layoutName,
					color: seriesColors[ii],
					data: analysis[ii].homeKeyWords,
					visible: true
				});
				homeBlockWordsData.push({
					label: analysis[ii].layoutName,
					color: seriesColors[ii],
					data: analysis[ii].homeBlockWords,
					visible: true
				});
				singleHandWordsData.push({
					label: analysis[ii].layoutName,
					color: seriesColors[ii],
					data: analysis[ii].singleHandWords,
					visible: true
				});
			}

			var cfuDisplayData = {};
			cfuDisplayData['nodups'] = [
				{
					label: 'Left Pinky',
					color: 'rgba(  0,255,255,0.5)',
					data: [KB.finger.LEFT_PINKY]
				},
				{
					label: 'Left Ring',
					color: 'rgba(  0,  0,230,0.5)',
					data: [KB.finger.LEFT_RING]
				},
				{
					label: 'Left Middle',
					color: 'rgba(136,136,255,0.5)',
					data: [KB.finger.LEFT_MIDDLE]
				},
				{
					label: 'Left Index',
					color: 'rgba(255,  0,255,0.5)',
					data: [KB.finger.LEFT_INDEX]
				},
				{
					label: 'Left Thumb',
					color: 'rgba(255,255,255,0.5)',
					data: [KB.finger.LEFT_THUMB]
				},
				{
					label: 'Right Thumb',
					color: 'rgba(204,204,204,0.5)',
					data: [KB.finger.RIGHT_THUMB]
				},
				{
					label: 'Right Index',
					color: 'rgba(255,  0,  0,0.5)',
					data: [KB.finger.RIGHT_INDEX]
				},
				{
					label: 'Right Middle',
					color: 'rgba(255,136,  0,0.5)',
					data: [KB.finger.RIGHT_MIDDLE]
				},
				{
					label: 'Right Ring',
					color: 'rgba(255,255,  0,0.5)',
					data: [KB.finger.RIGHT_RING]
				},
				{
					label: 'Right Pinky',
					color: 'rgba(  0,255,  0,0.5)',
					data: [KB.finger.RIGHT_PINKY]
				}
			];
			cfuDisplayData['dups'] = cfuDisplayData['nodups'];

			var chuDisplayData = {};
			chuDisplayData['nodups'] = [
				{
					label: 'Left Fingers',
					color: 'rgba(  0,  0,230,0.5)',
					data: ['left']
				},
				{
					label: 'Right Fingers',
					color: 'rgba(255,  0,  0,0.5)',
					data: ['right']
				}
			];
			chuDisplayData['dups'] = chuDisplayData['nodups'];

			var rollsInoutDisplayData = {};
			rollsInoutDisplayData['All'] = [
				{
					label: 'Inward',
					color: 'rgba(  0,  0,230,0.5)',
					data: ['in']
				},
				{
					label: 'Outward',
					color: 'rgba(255,  0,  0,0.5)',
					data: ['out']
				}
			];

			var modDisplayData = {};
			modDisplayData['all'] = [
				{
					label: 'Shift',
					color: 'rgba(  0,  0,230,0.5)',
					data: ['shift']
				},
				{
					label: 'AltGr',
					color: 'rgba(255,  0,  0,0.5)',
					data: ['altGr']
				},
				{
					label: 'Shift+AltGr',
					color: 'rgba(204,204,204,0.5)',
					data: ['shiftAltGr']
				},
				{
					label: 'Numpad',
					color: 'rgba(  0,204,  0,0.5)',
					data: ['numpad']
				}
			];

			var homeKeyWordsDisplayData = {};
			homeKeyWordsDisplayData['all'] = [
				{
					label: 'Words',
					color: 'rgba(  0,  0,230,0.5)',
					data: ['words']
				}
			];

			var cfuDisplayFilter = function (displayType, unitType, rawSeriesData, displayData) {
				var idx, sIndex = 0,
					ii, jj, items, val,
					seriesData = [],
					series,
					labels = [],
					seriesLabels = [],
					allSeriesLabels = [],
					colors = [],
					seriesColors = [];

				// separate out labels and colors
				for (ii = 0; ii < displayData[displayType].length; ii++) {
					labels.push(displayData[displayType][ii].label);
					colors.push(displayData[displayType][ii].color);
				}

				var rawData = rawSeriesData[displayType];
				for (idx = 0; idx < rawData.length; idx++) {
					allSeriesLabels.push(rawData[idx].label);
					if (!rawData[idx].visible) continue;

					seriesLabels.push(rawData[idx].label);
					seriesColors.push(rawData[idx].color);
					series = rawData[idx].data.slice(1);

					var total = 0;
					for (ii = 0; ii < series.length; ii++) {
						total += series[ii];
					}

					for (ii = 0; ii < series.length; ii++) {
						if (unitType === 'Percent') {
							series[ii] = (total > 0) ? (series[ii] / total) * 100 : 0;
							series[ii] = (!isFinite(series[ii])) ? 0 : series[ii];
						} else {
							series[ii] = unitConverter(series[ii], analysis[idx].pixelsPerCm, unitType, displayData[displayType][ii].data[0]);
						}
					}

					if (unitType === 'Percent') {
						total = 100;
					} else {
						total = 0;
						for (ii = 0; ii < series.length; ii++) {
							total += series[ii];
						}
					}
					series.total = total;
					seriesData[sIndex] = series;
					sIndex++;
				}
				seriesData.labels = labels;
				seriesData.colors = colors;
				seriesData.allSeriesLabels = allSeriesLabels;
				seriesData.seriesLabels = seriesLabels;
				seriesData.seriesColors = seriesColors;
				return seriesData;
			};

			var chuDisplayFilter = function (displayType, unitType, rawSeriesData, displayData) {
				var idx, sIndex = 0,
					ii, jj, items, val,
					seriesData = [],
					series,
					labels = [],
					seriesLabels = [],
					allSeriesLabels = [],
					colors = [],
					seriesColors = [];

				// separate out labels and colors
				for (ii = 0; ii < displayData[displayType].length; ii++) {
					labels.push(displayData[displayType][ii].label);
					colors.push(displayData[displayType][ii].color);
				}

				var rawData = rawSeriesData[displayType];
				for (idx = 0; idx < rawData.length; idx++) {
					allSeriesLabels.push(rawData[idx].label);
					if (!rawData[idx].visible) continue;

					seriesLabels.push(rawData[idx].label);
					seriesColors.push(rawData[idx].color);
					series = [];
					series[0] = rawData[idx].data['left'];
					series[1] = rawData[idx].data['right'];
					series.visible = rawData[idx].visible;

					var total = 0;
					for (ii = 0; ii < series.length; ii++) {
						total += series[ii];
					}

					for (ii = 0; ii < series.length; ii++) {
						if (unitType === 'Percent') {
							series[ii] = (total > 0) ? (series[ii] / total) * 100 : 0;
							series[ii] = (!isFinite(series[ii])) ? 0 : series[ii];
						} else {
							series[ii] = unitConverter(series[ii], analysis[idx].pixelsPerCm, unitType, displayData[displayType][ii].data[0]);
						}
					}

					if (unitType === 'Percent') {
						total = 100;
					} else {
						total = 0;
						for (ii = 0; ii < series.length; ii++) {
							total += series[ii];
						}
					}
					series.total = total;
					seriesData[sIndex] = series;
					sIndex++;
				}
				seriesData.labels = labels;
				seriesData.colors = colors;
				seriesData.allSeriesLabels = allSeriesLabels;
				seriesData.seriesLabels = seriesLabels;
				seriesData.seriesColors = seriesColors;

				return seriesData;
			};

			var modDisplayFilter = function (displayType, unitType, rawSeriesData, displayData) {
				var idx, sIndex = 0,
					ii, jj, items, val,
					seriesData = [],
					series,
					labels = [],
					seriesLabels = [],
					allSeriesLabels = [],
					colors = [],
					seriesColors = [];

				// separte out labels and colors
				for (ii = 0; ii < displayData[displayType].length; ii++) {
					labels.push(displayData[displayType][ii].label);
					colors.push(displayData[displayType][ii].color);
				}

				var rawData = rawSeriesData;
				for (idx = 0; idx < rawData.length; idx++) {
					allSeriesLabels.push(rawData[idx].label);
					if (!rawData[idx].visible) continue;

					seriesLabels.push(rawData[idx].label);
					seriesColors.push(rawData[idx].color);
					series = [];
					series[0] = rawData[idx].data['shift'];
					series[1] = rawData[idx].data['altGr'];
					series[2] = rawData[idx].data['shiftAltGr'];
					series[3] = rawData[idx].data['numpad'];
					series.visible = rawData[idx].visible;

					var total = 0;
					for (ii = 0; ii < series.length; ii++) {
						total += series[ii];
					}

					for (ii = 0; ii < series.length; ii++) {
						if (unitType === 'Percent') {
							series[ii] = (total > 0) ? (series[ii] / total) * 100 : 0;
							series[ii] = (!isFinite(series[ii])) ? 0 : series[ii];
						} else {
							series[ii] = unitConverter(series[ii], analysis[idx].pixelsPerCm, unitType, ii + 1);
						}
					}

					if (unitType === 'Percent') {
						total = 100;
					} else {
						total = 0;
						for (ii = 0; ii < series.length; ii++) {
							total += series[ii];
						}
					}
					series.total = total;
					seriesData[sIndex] = series;
					sIndex++;
				}
				seriesData.labels = labels;
				seriesData.colors = colors;
				seriesData.allSeriesLabels = allSeriesLabels;
				seriesData.seriesLabels = seriesLabels;
				seriesData.seriesColors = seriesColors;

				return seriesData;
			};

			var homeKeyWordsDisplayFilter = function (displayType, unitType, rawSeriesData, displayData) {
				var idx, sIndex = 0,
					ii, jj, items, val,
					seriesData = [],
					series,
					labels = [],
					seriesLabels = [],
					allSeriesLabels = [],
					colors = [],
					seriesColors = [];

				// separate out labels and colors
				for (ii = 0; ii < displayData[displayType].length; ii++) {
					labels.push(displayData[displayType][ii].label);
					colors.push(displayData[displayType][ii].color);
				}

				var rawData = rawSeriesData;
				for (idx = 0; idx < rawData.length; idx++) {
					allSeriesLabels.push(rawData[idx].label);
					if (!rawData[idx].visible) continue;

					seriesLabels.push(rawData[idx].label);
					seriesColors.push(rawData[idx].color);
					series = [];
					series[0] = rawData[idx].data['words'];
					series.total = rawData[idx].data['score'];
					series.visible = rawData[idx].visible;

					seriesData[sIndex] = series;
					sIndex++;
				}

				seriesData.labels = labels;
				seriesData.colors = colors;
				seriesData.allSeriesLabels = allSeriesLabels;
				seriesData.seriesLabels = seriesLabels;
				seriesData.seriesColors = seriesColors;

				// zip and sort, because stupidly too many separate arrays
				// zip
				var zipped = [];
				for (var i=0; i<seriesData.length; i++) {
					zipped.push({
						allSeriesLabels: seriesData.allSeriesLabels[i],
						seriesLabels: seriesData.seriesLabels[i],
						seriesColors: seriesData.seriesColors[i],
						series: seriesData[i],
					});
				}

				// sort
				zipped.sort(function (x, y) {
					return y.series.total - x.series.total;
				});

				// unzip
				var z;
				for (i=0; i<zipped.length; i++) {
					z = zipped[i];
						seriesData.allSeriesLabels[i] = z.allSeriesLabels;
						seriesData.seriesLabels[i] = z.seriesLabels;
						seriesData.seriesColors[i] = z.seriesColors;
						seriesData[i] = z.series;
				}

				return seriesData;
			};

			var rollsInoutDisplayFilter = function (displayType, unitType, rawSeriesData, displayData) {
				var idx, sIndex = 0,
					ii, jj, items, val,
					seriesData = [],
					series,
					labels = [],
					seriesLabels = [],
					allSeriesLabels = [],
					colors = [],
					seriesColors = [];


				// separate out labels and colors
				for (ii = 0; ii < displayData[displayType].length; ii++) {
					labels.push(displayData[displayType][ii].label);
					colors.push(displayData[displayType][ii].color);
				}

				var rawData = rawSeriesData;
				for (idx = 0; idx < rawData.length; idx++) {
					allSeriesLabels.push(rawData[idx].label);
					if (!rawData[idx].visible) continue;

					seriesLabels.push(rawData[idx].label);
					seriesColors.push(rawData[idx].color);
					series = [];
					series[0] = rawData[idx].data['in'];
					series[1] = rawData[idx].data['out'];
					series.visible = rawData[idx].visible;

					var total = 0;
					for (ii = 0; ii < series.length; ii++) {
						total += series[ii];
					}

					for (ii = 0; ii < series.length; ii++) {
						if (unitType === 'Percent') {
							series[ii] = (total > 0) ? (series[ii] / total) * 100 : 0;
							series[ii] = (!isFinite(series[ii])) ? 0 : series[ii];
						}
						else {
							series[ii] = unitConverter(series[ii], analysis[idx].pixelsPerCm, unitType, null);
						}
					}

					if (unitType === 'Percent') {
						total = 100;
					} else {
						total = 0;
						for (ii = 0; ii < series.length; ii++) {
							total += series[ii];
						}
					}
					series.total = total;
					seriesData[sIndex] = series;
					sIndex++;
				}

				seriesData.labels = labels;
				seriesData.colors = colors;
				seriesData.allSeriesLabels = allSeriesLabels;
				seriesData.seriesLabels = seriesLabels;
				seriesData.seriesColors = seriesColors;

				return seriesData;
			};
			// --------------------------------------------------------------------
			// Show results

			library.set('distance', {
				rawSeriesData: distSeriesData,
				displayFilter: displayFilter,
				displayType: 'All',
				displayData: displayData,
				units: 'Distance Penalty',
				allowedUnits: ['Distance Penalty', 'Centimeters', 'Meters', 'Feet', 'Miles', 'Percent']
			});

			library.set('rolls', {
				rawSeriesData: rollsSeriesData,
				displayFilter: displayFilter,
				displayType: 'All',
				displayData: displayData,
				units: 'Finger Penalty',
				allowedUnits: ['Finger Penalty', 'Key Presses', 'Percent']
			});

			library.set('rollsInout', {
				rawSeriesData: rollsInoutData,
				displayFilter: rollsInoutDisplayFilter,
				displayType: 'All',
				displayData: rollsInoutDisplayData,
				units: 'Key Presses',
				allowedUnits: ['Key Presses', 'Percent']
			});

			library.set('consecFinger', {
				rawSeriesData: cfuSeriesData,
				displayFilter: displayFilter,
				displayType: 'All',
				displayData: displayData,
				units: 'Finger Penalty',
				allowedUnits: ['Finger Penalty', 'Key Presses', 'Percent']
			});

			var chuData = {
				'nodups': chuSeriesData,
				'dups': chuidSeriesData
			};
			for (var i = 0; i < analysis.length; i++) {
				chuData[i] = {
					visible: true
				};
			}
			chuData.length = analysis.length;

			library.set('consecHandPress', {
				rawSeriesData: chuData,
				displayFilter: chuDisplayFilter,
				displayType: 'nodups',
				displayData: chuDisplayData,
				units: 'Key Presses',
				allowedUnits: ['Key Presses', 'Percent']
			});

			library.set('fingerUsage', {
				rawSeriesData: fuSeriesData,
				displayFilter: displayFilter,
				displayType: 'All',
				displayData: displayData,
				units: 'Finger Penalty',
				allowedUnits: ['Finger Penalty', 'Key Presses', 'Percent']
			});

			library.set('balance', {
				rawSeriesData: balanceSeriesData,
				displayFilter: displayFilter,
				displayType: 'All',
				displayData: displayData,
				units: 'Finger Penalty',
				allowedUnits: ['Finger Penalty', 'Percent']
			});

			library.set('homeKeyWords', {
				rawSeriesData: homeKeyWordsData,
				displayFilter: homeKeyWordsDisplayFilter,
				displayType: 'all',
				displayData: homeKeyWordsDisplayData,
				units: 'Words',
				allowedUnits: ['Words']
			});

			library.set('homeBlockWords', {
				rawSeriesData: homeBlockWordsData,
				displayFilter: homeKeyWordsDisplayFilter,
				displayType: 'all',
				displayData: homeKeyWordsDisplayData,
				units: 'Words',
				allowedUnits: ['Words']
			});

			library.set('singleHandWords', {
				rawSeriesData: singleHandWordsData,
				displayFilter: homeKeyWordsDisplayFilter,
				displayType: 'all',
				displayData: homeKeyWordsDisplayData,
				units: 'Words',
				allowedUnits: ['Words']
			});

			library.set('rowUsage', {
				rawSeriesData: rowSeriesData,
				displayFilter: displayFilter,
				displayType: 'All',
				displayData: rowDisplayData,
				units: 'Key Presses',
				allowedUnits: ['Key Presses', 'Percent']
			});

			library.set('modifierUse', {
				rawSeriesData: modSeriesData,
				displayFilter: modDisplayFilter,
				displayType: 'all',
				displayData: modDisplayData,
				units: 'Key Presses',
				allowedUnits: ['Key Presses', 'Percent']
			});

			library.set('layouts', kLayouts);
			library.set('keyData', keyData);

			function countCharFreq(text) {
				// count character frequency
				// vars to use: text

				text = text.replace(/\r\n/g, "\r").replace(/\n/g, "\r");
				var textSize = text.length;
				var charFreq = {};

				// store in temporary object
				for (var i=0; i < textSize; i++) {
					var char = text.charCodeAt(i);
					if (charFreq[char]) {
						charFreq[char] += 1;
					} else {
						charFreq[char] = 1;
					}
				}

				// convert to array that can be sorted
				var charFreqData = [];
				for (prop in charFreq) {
					if (charFreq.hasOwnProperty(prop)) {
						charFreqData.push({
							'character': KB.Key.labels[prop] ? KB.Key.labels[prop] : String.fromCharCode(prop),
							'count': charFreq[prop],
							'percent': charFreq[prop] / textSize,
						});
					}
				}

				// sort by count, high to low
				charFreqData.sort(
					function(a, b) {
						return b.count - a.count;
					}
				);

				return charFreqData;
			}

			function countBigramFreq(text) {
				// count bigram frequency
				// vars to use: text

				text = text.replace(/\r\n/g, "⏎").replace(/\n/g, "⏎").replace(/ /g, "␣").replace(/\t/g, "↹");
				var textSize = text.length;
				var bigramFreq = {};

				// store in temporary object
				for (var i=0; i < textSize - 1; i++) {
					var bigram = text.charAt(i) + '' + text.charAt(i+1);
					if (bigramFreq[bigram]) {
						bigramFreq[bigram] += 1;
					} else {
						bigramFreq[bigram] = 1;
					}
				}

				// convert to array that can be sorted
				var bigramFreqData = [];
				 for (prop in bigramFreq) {
					  if (bigramFreq.hasOwnProperty(prop)) {
							bigramFreqData.push({
								 'bigram': prop,
								 'count': bigramFreq[prop],
								 'percent': bigramFreq[prop] / textSize,
							});
					  }
				 }

				// sort by count, high to low
				bigramFreqData.sort(
					function(a, b) {
						return b.count - a.count;
					}
				);

				return bigramFreqData;
			}

			library.set('charFreqData', countCharFreq(txt));
			library.set('bigramFreqData', countBigramFreq(txt));

			function getSynopsis(text) {
				return text.substring(0, 288);
			}

			library.set('synopsis', getSynopsis(txt));

			return true;
		};

		return me;
}

]);
/*
Service for storing globally available data
*/

var appServices = appServices || angular.module('kla.services', []);

appServices.factory('textPresets', ['$http',
function ($http) {
		var service = {};

		service.load = function (corpus) {
			var promise = $http.get('../corpus/' + corpus + '.txt').then(function (response) {
				return response.data;
			});
			return promise;
		};

		return service;
}

]);
