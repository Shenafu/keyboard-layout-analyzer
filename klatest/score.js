
var Score = Score || {}; // define namespace

// class used to calculate the final score for a layout after all analysis completed
// score categories are:
// distance, fingerUsage, rolls, sameFinger, sameHand, words

// DISTANCE
Score.distance = function (analysis, category) {
	var total = 0;
	for (var finger in KB.fingers) {
		if (KB.fingers.hasOwnProperty(finger) && KB.dScoring[finger]) {
			total += (analysis.distance[finger]) * (KB.dScoring[finger]) * KB.finger.gethScoring(finger);
		}
	}

	return category.weight * category.penalty * total / analysis.tLen;
};

// FINGER USAGE
Score.fingerUsage = function (analysis, category) {
	var total = 0;
	for (var finger in KB.fingers) {
		if (KB.fingers.hasOwnProperty(finger) && KB.fScoring[finger]) {
			total += (analysis.fingerUsage[finger] * KB.fScoring[finger]) * KB.finger.gethScoring(finger);
		}
	}

	return category.weight * category.penalty * total / analysis.tLen;
};

// ROLLS
Score.rolls = function (analysis, category) {
	var total = 0;
	for (var finger in KB.fingers) {
		if (KB.fingers.hasOwnProperty(finger) && KB.fScoring[finger]) {
			total += analysis.rolls[finger] * Math.pow(KB.fScoring[finger], 2) * KB.finger.gethScoring(finger);
		}
	}

	return category.weight * category.penalty * total * analysis.rollsInout['out'] / (analysis.rollsInout['in'] || 1) / ((analysis.rollsInout['out'] + analysis.rollsInout['in']) || 1);
};

// SAME FINGER
Score.sameFinger = function (analysis, category) {
	var total = 0;
	for (var finger in KB.fingers) {
		if (KB.fingers.hasOwnProperty(finger) && KB.fScoring[finger]) {
			total += analysis.consecFingerRowJump[finger] * KB.fScoring[finger] * KB.finger.gethScoring(finger);
		}
	}

	return category.weight * category.penalty * total / analysis.tLen;
};

// SAME HAND
Score.sameHand = function (analysis, category) {
	var total = (analysis.consecHandPress.left * KB.hScoring[KB.hand.LEFT_HAND] + analysis.consecHandPress.right * KB.hScoring[KB.hand.RIGHT_HAND]);

	return category.weight * category.penalty * total * total / analysis.tLen / analysis.tLen;
};

// WORDS
Score.words = function (analysis, category) {
//	var total = analysis.homeBlockWords.score ? (analysis.singleHandWords.score / (analysis.homeBlockWords.score)) : 0;

//	var total = (category.numWords * 0.8 + (analysis.singleHandWords.score - analysis.homeBlockWords.score));

	var total = (Math.pow( Math.max(category.numWords - analysis.homeBlockWords.score, 0), 1.70) + Math.pow(/*category.numWords + */analysis.singleHandWords.score, 1.70));

//console.log("singleHandWords: " + analysis.singleHandWords.score);
//console.log("homeBlockWords: " + analysis.homeBlockWords.score);
//console.log("total: " + total);

	return category.weight * category.penalty * total / (Math.pow(category.numWords, 1.7) || 1);

};

// BALANCE
Score.balance = function (analysis, category) {
	var total = 0;
	var diff = 0;
	var ideal = 0;

	for (var finger in KB.fingers) {
		if (KB.fingers.hasOwnProperty(finger) && KB.fScoring[finger]) {
			analysis.balance[finger] = (analysis.fingerUsage[finger] * KB.fScoring[finger]) * KB.finger.gethScoring(finger);
			total += analysis.balance[finger];
		}
	}

	for (var finger in KB.fingers) {
		ideal = total * KB.ideal[finger];
		diff = analysis.balance[finger] - ideal;
		analysis.balance[finger] = Math.max(diff, 0);
	}

	total = 0;
	for (var finger in KB.fingers) {
		total += analysis.balance[finger] ? analysis.balance[finger] : 0;
	}

	return category.weight * category.penalty * total /analysis.tLen;

};
