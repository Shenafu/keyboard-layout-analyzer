/* jshint node: true */
/* globals KB, $, angular */

/*
	Controller for the compare route
*/
var appControllers = appControllers || angular.module('kla.controllers', []);

appControllers.controller('LayoutCtrl', ['$scope', '$http', '$timeout', '$log', 'keyboards',
	function ($scope, $http, $timeout, $log, keyboards) {
		$scope.current = 0;
		$scope.keyboards = keyboards;

		$scope.switchLayout = function (evt, start, idx) {
			$scope.current = idx;
		};

		$scope.showImportDialog = function () {
			$('#kb-config-import-dialog .kb-config-dialog-txt').val("");
			$('#kb-config-import-dialog').modal('show');
		};

		$scope.showExportDialog = function () {
			var val = JSON.stringify(keyboards.getKeySet($scope.current),  undefined, 4);

			$('#kb-config-export-dialog').modal('show');
			$('#kb-config-export-dialog .kb-config-dialog-txt').val(val);
			setTimeout($scope.selectAllExportText, 660);

			var link = document.getElementById("layout-download-link");
			link.href = "data:text/text;," + val;
			var fn = keyboards.getKeySet($scope.current).label + "." + keyboards.getKeySet($scope.current).keyboardType;
			fn = fn.trim().toLowerCase().replace(/ /g, "-");
			link.download = fn + ".json";
		};

		$scope.showVsvDialog = function () {
			var val = keyboards.toVsv($scope.current);
			$('#kb-config-export-dialog').modal('show');
			$('#kb-config-export-dialog .kb-config-dialog-txt').val(val);
			setTimeout($scope.selectAllExportText, 660);

			var link = document.getElementById("layout-download-link");
			link.href = "data:text/text;," + val;
			var fn = keyboards.getKeySet($scope.current).label + "." + keyboards.getKeySet($scope.current).keyboardType;
			fn = fn.trim().toLowerCase().replace(/ /g, "-");
			link.download = fn + ".vsv";
		};

		$scope.selectAllExportText = function () {
			$('#kb-config-export-dialog .kb-config-dialog-txt').select();
			try {
				document.execCommand('copy');
			} catch (e) {
				// Opera: DOMException SECURITY_ERR
				console.log(e);
			}
		};

		$scope.makeImage = function () {
			// generate PNG file from canvas

			var canvas = document.getElementsByTagName("keyboardeditor")[$scope.current].children[0].children[0];
			var img = document.getElementById("generated-image");
			var link = document.getElementById("generated-image-link");

			img.src = link.href = canvas.toDataURL();
			img.alt = keyboards.getKeySet($scope.current).label;
			link.download = keyboards.getKeySet($scope.current).label + ".png";
		};

		$scope.isMoreInfo = function (keySet) {
			if (typeof keySet.moreInfoUrl === 'undefined') {
				return false;
			}
			if (typeof keySet.moreInfoText === 'undefined') {
				return false;
			}
			if (keySet.moreInfoUrl === '' || keySet.moreInfoText === '') {
				return false;
			}
			return true;
		};

		$scope.importLayout = function () {
			var res = keyboards.parseKeySet($('#kb-config-import-dialog .kb-config-dialog-txt').val());
			if (res.valid) {
				keyboards.setLayout($scope.current, {
					keySet: $.extend(true, {}, res.keySet),
					keyMap: $.extend(true, {}, keyboards.getKeyMapFromKeyboardType(res.keySet.keyboardType))
				});
				$('#kb-config-import-dialog').modal('hide');
			} else {
				alert(res.reason);
			}
		};

		$scope.loadLayout = function (file, index) {
			file = file || $('#kb-config-select-list').find('option:selected').attr('value');
			index = index || $scope.current;

			var val = file.split(".");
			var name = val[0], type = val[1], ext = val[2];
			if (typeof KB.keySet[type] !== 'undefined' && typeof KB.keySet[type][name] !== 'undefined') {
				keyboards.setLayout(index, {
					keySet: $.extend(true, {}, KB.keySet[type][name]),
					keyMap: $.extend(true, {}, KB.keyMap[type].s683_225)
				});
			} else {
				// retrieve layout from server
				$http.get('../layouts/' + file).then(
					function (response) {
						var res = keyboards.parseKeySet(response.data);
						if (res.valid) {
							res.keySet.keyboardType = type;
							keyboards.setLayout(index, {
								keySet: res.keySet,
								keyMap: KB.keyMap[type].s683_225
							});
						} else {
							alert(res.reason);
						}
					},
					function (response) {
						// error loading layout
						alert('Unexpected Error. Layout not found; not loaded.');
					}
				);
			}
		};

	}
]);

appControllers.controller('LoadCtrl', ['$scope', '$routeParams', '$location', '$http', '$timeout', '$log', 'keyboards', 'library', 'resultsGenerator',
	function ($scope, $routeParams, $location, $http, $timeout, $log, keyboards, library, resultsGenerator) {

		var analyzeData = function (txt) {
			try {
				if (resultsGenerator.go(txt)) {
					$location.path('/results');
				}
			} catch (err) {
				alert(err.message);
				$location.path('/compare');
			}
		};

		$scope.$on('$viewContentLoaded', function () {

			$timeout(function () {
				var textKey = $routeParams.textKey;
				if (typeof textKey === 'undefined') {
					analyzeData(library.get('input-text'));
				} else {
					$http({
							method: 'POST',
							url: '../api/load-results.php',
							data: {
								textKey: $routeParams.textKey
							}
						})
						.success(function (data, status, headers, config) {
							$log.debug('success!!!');
							$log.debug(data);

							var layouts = data.layoutText;
							var ii;
							for (ii = 0; ii < layouts.length; ii++) {
								var res = keyboards.parseKeySet(layouts[ii].keySet);
								if (res.valid) {
									keyboards.setLayout(ii, {
										keySet: $.extend(true, {}, res.keySet),
										keyMap: $.extend(true, {}, keyboards.getKeyMapFromKeyboardType(res.keySet.keyboardType))
									});
								} else {
									$log.debug('unable to load layout');
									$log.debug(res);
								}
							}

							library.set('input-text', data.inputText);
							library.set('textKey', textKey);
							analyzeData(library.get('input-text'));

						})
						.error(function (data, status, headers, config) {
							$location.path('/compare');
						});
				}
			}, 500);


		});
	}
]);

appControllers.controller('CompareCtrl', ['$scope', '$location', 'library', 'resultsGenerator', 'textPresets',
	function ($scope, $location, library, resultsGenerator, textPresets) {

		$scope.data = {};
		$scope.data.text = library.get('input-text');
		$scope.corpora = DB.corpora;
		$scope.data.textPreset = '';

		$scope.updateDropdown = function () {
			if (typeof $scope.data.textPreset === 'string') {
				for (var i=0, c=$scope.corpora, len=c.length; i < len; i++) {
					if (c[i].file === $scope.data.textPreset) {
						$scope.data.textPreset = c[i];
						break;
					}
				}
			}
		};

		$scope.applyPreset = function () {
			if ($scope.data.textPreset === '') return;
			$scope.data.text = "Loading, one moment please...";
			textPresets.load($scope.data.textPreset).then(function (res) {
				$scope.data.text = res;
			});
			$scope.updateDropdown();
		};

		// apply sample text
		if (typeof $scope.data.text === 'undefined') {
			$scope.data.textPreset = $scope.corpora[0].file;
			$scope.applyPreset();
		}

		$scope.generateOutput = function (txt) {
			if (txt === '') {
				alert('Please enter in some text to analyze.');
				return;
			}

			library.set('input-text', txt);
			$location.path('/load');
		};

		$scope.$watch('data.text', function (newVal, oldVal) {
			library.set('input-text', $scope.data.text);
		}, true);

		$scope.onKeyPress = function () {
			switch(event.key) {
				case "Enter":
					$scope.generateOutput($scope.data.text);
					break;

				default:
					break;
			}
		}

	}
]);

appControllers.controller('ResultsCtrl', ['$scope', '$location', '$http', '$log', 'library',
	function ($scope, $location, $http, $log, library) {

		$scope.results = library.get();
		$scope.settings = {};
		$scope.settings.chuIgnoreDups = true;
		$scope.currentHeatmap = 0;
		$scope.share = {};
		$scope.share.showSection = true;
		$scope.share.generatingUrl = false;
		$scope.share.url = '';
		$scope.share.returnedUrl = '';

		console.log('results');
		console.dir($scope.results);

		// If no result data exist, redirect to the compare page
		if (typeof $scope.results.distance === 'undefined') {
			$location.path('/compare');
			return;
		}

		if ($scope.results['input-text'].length > 500000) {
			$scope.share.showSection = false;
		}

		function getShareUrl(textKey) {
			return window.location.protocol + '//' + window.location.host + window.location.pathname + '#/load/' + textKey;
		}

		function isNumber(n) {
			return !isNaN(parseFloat(n)) && isFinite(n);
		}

		$scope.getUrlToShare = function () {
			if ($scope.share.returnedUrl !== '') {
				$scope.share.url = $scope.share.returnedUrl;
				return;
			}
			if ($scope.share.generatingUrl === true) return;
			$scope.share.generatingUrl = true;
			$scope.share.url = 'One moment please...';

			var layouts = $.extend(true, [], $scope.results.layouts);

			$http({
					method: 'POST',
					url: '../api/save-results.php',
					data: {
						inputText: $scope.results.inputText,
						layoutText: JSON.stringify(layouts)
					}
				})
				.success(function (data, status, headers, config) {
					$log.debug('success in saving data');
					$log.debug(data);
					$scope.share.url = getShareUrl(data.textKey);
					$scope.share.returnedUrl = $scope.share.url;
					$scope.share.generatingUrl = false;
				})
				.error(function (data, status, headers, config) {
					$log.debug('failed to save data');
					$log.debug(data);
					$scope.share.url = 'Unable to save data, try again in an hour or two.';
					$scope.share.generatingUrl = false;
				});
		};

		$scope.getKeyLabel = function (primary, shift, altGr, shiftAltGr, numpad) {
			var ret = '',
				maxPushTypes = Object.keys(KB.PUSH_TYPES).length, // types of modifiers, plus the primary type
				newVal, keyCode;
			for (var ii = 0; ii < maxPushTypes; ii++) {
				keyCode = arguments[ii] || -1; // unset modifiers gets -1
				if (isNumber(keyCode)) {
					newVal = (typeof KB.Key.labels[keyCode] !== 'undefined') ? KB.Key.labels[keyCode] : String.fromCharCode(keyCode);
					ret = (ii === 0) ? newVal : ret + ' ' + newVal;
				}
			}
			return ret;
		};

		$scope.switchHeatmap = function (evt, start, idx) {
			$scope.currentHeatmap = idx;
		};

		var processInputData = function (tab) {
			var lookup = {},
				ii;
			lookup.distance = ['distance'];
			lookup.fingerUsage = ['fingerUsage'];
			lookup.rolls = ['rolls', 'rollsInout'];
			lookup.rollsInout = ['rollsInout'];
			lookup.consecutive = ['consecFinger', 'consecHandPress'];
			lookup.consecFinger = ['consecFinger'];
			lookup.consecHandPress = ['consecHandPress'];
			lookup.balance = ['balance'];
			lookup.rowUsage = ['rowUsage'];
			lookup.words = ['homeKeyWords', 'homeBlockWords', 'singleHandWords'];
			lookup.homeKeyWords = ['homeKeyWords'];
			lookup.homeBlockWords = ['homeBlockWords'];
			lookup.singleHandWords = ['singleHandWords'];
			lookup.miscellaneous = ['modifierUse'];
			lookup.modifierUse = ['modifierUse'];

			if (!lookup[tab]) return;

			for (ii = 0; ii < lookup[tab].length; ii++) {
				var prop = lookup[tab][ii];
				$scope.results[prop].seriesData = $scope.results[prop].displayFilter($scope.results[prop].displayType,
					$scope.results[prop].units,
					$scope.results[prop].rawSeriesData,
					$scope.results[prop].displayData);
				$scope.results[prop].dirty = Date.now();
			}
		};

		$scope.returnToInput = function () {
			$location.path('/compare');
		};

		$scope.tabSwitch = function (evt, tab) {
			evt.preventDefault();
			$(evt.currentTarget).tab('show');
			$log.debug('tab switch!' + tab);
			processInputData(tab);
		};

		$scope.selectRow = function (evt) {
			var row = $(evt.currentTarget);
			if (row.hasClass("highlight") ) {
				row.removeClass("highlight");
			}
			else {
				row.addClass("highlight");
			}
		};

		/*
		    Init and watches
		*/

		// update share url
		if ($scope.results.textKey) {
			$scope.share.url = getShareUrl($scope.results.textKey);
			$scope.share.returnedUrl = $scope.share.url;
			library.set('textKey', null); // for future uses
		}

		var seriesTypes = ['distance', 'fingerUsage', 'rolls', 'rollsInout', 'balance', 'rowUsage', 'consecFinger', 'consecHandPress', 'modifierUse', 'homeKeyWords', 'homeBlockWords', 'singleHandWords'];
		var ii = 0;
		for (ii = 0; ii < seriesTypes.length; ii++) {
			var prop = seriesTypes[ii];
			$scope.$watch('results["' + prop + '"].units + results["' + prop + '"].displayType', (function () {
				var myProp = prop;
				return function (newVal, oldVal) {
					processInputData(myProp);
				};
			})(), true);

			$scope.$watch('results["' + prop + '"].rawSeriesData', (function () {
				var myProp = prop;
				return function (newVal, oldVal) {
					processInputData(myProp);
				};
			})(), true);
		}

		$scope.$watch('settings.chuIgnoreDups', function (newVal, oldVal, scope) {
			scope.results.consecHandPress.displayType = (newVal === true) ? 'nodups' : 'dups';
		});
	}
]);

appControllers.controller('CorpusCtrl', ['$scope',
	function ($scope) {
		$scope.corpora = DB.corpora;
	}
]);

appControllers.controller('LoadLayoutCtrl', ['$scope',
	function ($scope) {
		$scope.layouts = DB.layouts;
	}
]);