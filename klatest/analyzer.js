/* jshint node: true */
/* globals KB, angular */

"use strict";

var KLA = KLA || {};

if (typeof console === "undefined") {
	var console = {};
	console.log = function () {
		return;
	};
	console.dir = function () {
		return;
	};
}

KLA.Analyzer = (function () {

	var me;
	var distanceBetweenKeysCached;

	me = function () {
		return me;
	};

	// utility function
	var memoize = function (funct) {
		var cache = {};
		return function (object) {
			var key = arguments.length + Array.prototype.join.call(arguments, ",");
			if (key in cache) {
				return cache[key];
			} else {
				cache[key] = funct.apply(object, arguments);
				return cache[key];
			}
		};
	};

	function distanceBetweenKeys(keyMap, keyIndex1, keyIndex2) {
		var xDiff = (keyMap[keyIndex1].cx - keyMap[keyIndex2].cx);
		var yDiff = keyMap[keyIndex1].cy - keyMap[keyIndex2].cy;

		xDiff *= 1.1;

		if (yDiff > 0) {
			yDiff *= 1.1;
		}

		return Math.sqrt(xDiff * xDiff + yDiff * yDiff);
	}

	/*
	Look up where the char was typed and any other information we'll need.

	Input:
	* keySet
	* charCode

	An object with the following properties will be returned:
	* fingerUsed          - Represents the finger used.
	* keyIndex            - keyID for the keySet
	* charCode            - Char code for character pressed
	* pushType            - What type of key (ie, KB.PRIME_PUSH, KB.SHIFT_PUSH, etc)
	* errors              - A string array indicating any errors
	*/
	function findCharInKeySet(keySet, charCode) {
		var len;
		var ii;
		//var hand; // unused var
		var keys = keySet.keys;
		var ret = {
			fingerUsed: null,
			keyIndex: null,
			charCode: charCode,
			pushType: null,
			errors: []
		};

		//console.log("Entering findCharInKeySet");

		len = keys.length;
		for (ii = 0; ii < len; ii++) {
			if (keys[ii].primary && keys[ii].primary === charCode) {
				ret.fingerUsed = keys[ii].finger;
				ret.keyIndex = ii;
				ret.pushType = KB.PRIME_PUSH;
				break;
			} else if (keys[ii].shift && keys[ii].shift === charCode) {
				ret.fingerUsed = keys[ii].finger;
				ret.keyIndex = ii;
				ret.pushType = KB.SHIFT_PUSH;
				break;
			} else if (keys[ii].altGr && keys[ii].altGr === charCode) {
				ret.fingerUsed = keys[ii].finger;
				ret.keyIndex = ii;
				ret.pushType = KB.ALTGR_PUSH;
				break;
			} else if (keys[ii].shiftAltGr && keys[ii].shiftAltGr === charCode) {
				ret.fingerUsed = keys[ii].finger;
				ret.keyIndex = ii;
				ret.pushType = KB.SHIFT_ALTGR_PUSH;
				break;
			} else if (keys[ii].numpad && keys[ii].numpad === charCode) {
				ret.fingerUsed = keys[ii].finger;
				ret.keyIndex = ii;
				ret.pushType = KB.NUMPAD_PUSH;
				break;
			}
		}

		// Blogs often change certain characters to other codes, so do some character changes to accommodate certain instances
		if (ret.fingerUsed === null) {
			switch (charCode) {
			case 8217:
				return findCharInKeySet(keySet, 39); // '
			case 8220:
			case 8221:
				return findCharInKeySet(keySet, 34); // "
			case 8211:
				return findCharInKeySet(keySet, 45); // -
			}
			ret.errors.push("Char code not found: 0x " + charCode.toString(16).toUpperCase() + " (" + String.fromCharCode(charCode) + ")");
		}

		if (ret.pushType !== KB.PRIME_PUSH && (charCode === 16 || charCode === -16 || charCode === -18)) {
			//console.log("Shift Key and AltGR Key can only be set as 'primary' key presses.");
			ret.errors.push("Shift Key and AltGR Key can only be set as 'primary' key presses.");
			return ret;
		}

		//console.dir(ret);
		//console.log("Leaving findCharInKeySet");

		return ret;
	}

	/*
	Returns an array of the fingers used to press the key.
	An object from the "findCharInKeySet" function is taken in as input.
	*/
	function getFingersUsed(char2KeyMap, keyInfo) {
		var hand = KB.finger.whichHand(keyInfo.fingerUsed),
			fingers = {},
			shiftInfo,
			altGrInfo,
			numpadInfo;

		//console.log("Entering getFingersUsed");

		fingers[keyInfo.fingerUsed] = true;

		if (keyInfo.pushType === KB.SHIFT_PUSH) {
			shiftInfo = (hand === "right") ? char2KeyMap[16] : char2KeyMap[-16];
			fingers[shiftInfo.fingerUsed] = true;
		} else if (keyInfo.pushType === KB.ALTGR_PUSH) {
			altGrInfo = char2KeyMap[-18];
			//console.dir(altGrInfo);
			fingers[altGrInfo.fingerUsed] = true;
		} else if (keyInfo.pushType === KB.SHIFT_ALTGR_PUSH) {
			shiftInfo = (hand === "right") ? char2KeyMap[16] : char2KeyMap[-16];
			altGrInfo = char2KeyMap[-18];
			fingers[shiftInfo.fingerUsed] = true;
			fingers[altGrInfo.fingerUsed] = true;
		} else if (keyInfo.pushType === KB.NUMPAD_PUSH) {
			numpadInfo = char2KeyMap[19];
			fingers[numpadInfo.fingerUsed] = true;
		}

		//console.log("Leaving getFingersUsed");

		return fingers;
	}

	/*
	input:
	keyMap             - key map
	fingerHomes        - object indexed by fingers, maps to key index where they start
	fingerPositions    - current finger positions (same type of object as fingerHomes)
	except             - finger indexes to not return to home keys
	analysis           - analysis object
	*/
	function returnFingersToHomeRow(keyMap, fingerHomes, fingerPositions, analysis, except) {

		var finger;

		//console.log("Entering returnFingersToHomeRow");

		for (finger in KB.fingers) {
			if (KB.fingers.hasOwnProperty(finger)) {
				if (except && except[finger]) {
					continue;
				} // don't return finger if in except list
				if (fingerHomes[finger] === fingerPositions[finger]) {
					continue;
				} // finger already home
				//console.log("finger:"+finger);
				analysis.distance[finger] += distanceBetweenKeysCached(keyMap, fingerPositions[finger], fingerHomes[finger]);
				fingerPositions[finger] = fingerHomes[finger]; // return finger to key
			}
		}

		//console.log("Leaving returnFingersToHomeRow");
	}

	/*
	Inputs:
	keyInfo: char2KeyMap[charCode],
	char2KeyMap:
	fingerPositions: curFingerPos,
	keyMap: keyMap,
	analysis: analysis
	*/
	function typeKey(config) {

		var keyInfo = config.keyInfo,
			char2KeyMap = config.char2KeyMap,
			fingerPositions = config.fingerPositions,
			keyMap = config.keyMap,
			keySet = config.keySet,
			analysis = config.analysis,
			hand = KB.finger.whichHand(keyInfo.fingerUsed),
			shiftInfo = {},
			altGrInfo = {},
			numpadInfo = {},
			errors = [],
			tmpHand;

		//console.log("Entering typeKey");

		switch (keyInfo.pushType) {
		case KB.SHIFT_PUSH:
			shiftInfo = (hand === "right") ? char2KeyMap[16] : char2KeyMap[-16];
			break;
		case KB.ALTGR_PUSH:
			altGrInfo = char2KeyMap[-18];
			break;
		case KB.SHIFT_ALTGR_PUSH:
			shiftInfo = (hand === "right") ? char2KeyMap[16] : char2KeyMap[-16];
			altGrInfo = char2KeyMap[-18];
			break;
		case KB.NUMPAD_PUSH:
			numpadInfo = char2KeyMap[19];
			break;
		}

		//shift key has been lifted up
		if (
			(!angular.equals(analysis.tmp.prevShiftInfo, {}) && !angular.equals(shiftInfo.keyIndex, analysis.tmp.prevShiftInfo.keyIndex))
		) {
			analysis.tmp.prevFingerUsed = analysis.tmp.prevShiftInfo.fingerUsed;
			analysis.tmp.prevHandUsed = KB.finger.leftRightOrThumb(analysis.tmp.prevShiftInfo.fingerUsed);
			analysis.tmp.prevKeyIndex = analysis.tmp.prevShiftInfo.keyIndex;
		}

		// shift key has gone down
		if (
			(!angular.equals(shiftInfo, {})) &&
			(angular.equals(analysis.tmp.prevShiftInfo, {}) ||
			!angular.equals(shiftInfo.keyIndex, analysis.tmp.prevShiftInfo.keyIndex))
		) {
			analysis.distance[shiftInfo.fingerUsed] += moveFingerToKey(keyMap, fingerPositions, shiftInfo);
			analysis.distance[shiftInfo.fingerUsed] += KB.MODIFIERPENALTY;
			analysis.fingerUsage[shiftInfo.fingerUsed]++;
			analysis.rowUsage[keyMap[shiftInfo.keyIndex].row]++;
			analysis.keyData[shiftInfo.keyIndex].count++;
			analysis.numKeys++;
			analysis.modifierUse.shift++;

			if (analysis.tmp.prevFingerUsed === shiftInfo.fingerUsed) {
				var drow = Math.abs(keyMap[shiftInfo.keyIndex].row - keyMap[analysis.tmp.prevKeyIndex].row) + 1;
				var dx = 0.25 * Math.pow(drow, 2);
				analysis.consecFingerRowJump[shiftInfo.fingerUsed] += dx;
			}
			analysis.tmp.prevFingerUsed = shiftInfo.fingerUsed;

			tmpHand = KB.finger.leftRightOrThumb(shiftInfo.fingerUsed);
			if (analysis.tmp.prevHandUsed === tmpHand) {
				if (analysis.tmp.prevKeyIndex !== shiftInfo.keyIndex) {
					analysis.consecHandPressIgnoreDups[tmpHand]++;
				}
				analysis.consecHandPress[tmpHand]++;
			}
			analysis.tmp.prevHandUsed = tmpHand;

			analysis.tmp.prevKeyIndex = shiftInfo.keyIndex;
		} else if (typeof shiftInfo.fingerUsed !== "undefined") {
			analysis.modifierUse.shift++;
			analysis.fingerUsage[shiftInfo.fingerUsed]++;
			analysis.distance[shiftInfo.fingerUsed] += KB.MODIFIERPENALTY;
			analysis.numKeys++;
		}

		// altgr key has been lifted up
		if (
			(!angular.equals(analysis.tmp.prevAltGrInfo, {}) && !angular.equals(altGrInfo, analysis.tmp.prevAltGrInfo))
		) {
			analysis.tmp.prevFingerUsed = analysis.tmp.prevAltGrInfo.fingerUsed;
			analysis.tmp.prevHandUsed = KB.finger.leftRightOrThumb(analysis.tmp.prevAltGrInfo.fingerUsed);
			analysis.tmp.prevKeyIndex = analysis.tmp.prevAltGrInfo.keyIndex;
		}

		// altgr key has gone down
		if (
			(!angular.equals(altGrInfo, {})) &&
			(angular.equals(analysis.tmp.prevAltGrInfo, {}) ||
			!angular.equals(altGrInfo, analysis.tmp.prevAltGrInfo))
		) {

			analysis.distance[altGrInfo.fingerUsed] += moveFingerToKey(keyMap, fingerPositions, altGrInfo);
			analysis.distance[altGrInfo.fingerUsed] += KB.MODIFIERPENALTY;
			analysis.fingerUsage[altGrInfo.fingerUsed]++;
			analysis.rowUsage[keyMap[altGrInfo.keyIndex].row]++;
			analysis.keyData[altGrInfo.keyIndex].count++;
			analysis.numKeys++;
			analysis.modifierUse.altGr++;

			if (analysis.tmp.prevFingerUsed === altGrInfo.fingerUsed) {
				var drow = Math.abs(keyMap[altGrInfo.keyIndex].row - keyMap[analysis.tmp.prevKeyIndex].row) + 1;
				var dx = 0.25 * Math.pow(drow, 2);
				analysis.consecFingerRowJump[altGrInfo.fingerUsed] += dx;
			}
			analysis.tmp.prevFingerUsed = altGrInfo.fingerUsed;

			tmpHand = KB.finger.leftRightOrThumb(altGrInfo.fingerUsed);
			if (analysis.tmp.prevHandUsed === tmpHand) {
				if (analysis.tmp.prevKeyIndex !== altGrInfo.keyIndex) {
					analysis.consecHandPressIgnoreDups[tmpHand]++;
				}
				analysis.consecHandPress[tmpHand]++;
			}
			analysis.tmp.prevHandUsed = tmpHand;

			analysis.tmp.prevKeyIndex = altGrInfo.keyIndex;

		} else if (typeof altGrInfo.fingerUsed !== "undefined") {
			analysis.modifierUse.altGr++;
			analysis.fingerUsage[altGrInfo.fingerUsed]++;
			analysis.distance[altGrInfo.fingerUsed] += KB.MODIFIERPENALTY;
			analysis.numKeys++;
		}

		// numpad key has been lifted up
		if (
			(!angular.equals(analysis.tmp.prevNumpadInfo, {}) && !angular.equals(numpadInfo, analysis.tmp.prevNumpadInfo))
		) {
			analysis.tmp.prevFingerUsed = analysis.tmp.prevNumpadInfo.fingerUsed;
			analysis.tmp.prevHandUsed = KB.finger.leftRightOrThumb(analysis.tmp.prevNumpadInfo.fingerUsed);
			analysis.tmp.prevKeyIndex = analysis.tmp.prevNumpadInfo.keyIndex;
		}

		// numpad key has gone down
		if (
			(!angular.equals(numpadInfo, {})) &&
			(angular.equals(analysis.tmp.prevNumpadInfo, {}) ||
			!angular.equals(numpadInfo, analysis.tmp.prevNumpadInfo))
		) {

			analysis.distance[numpadInfo.fingerUsed] += moveFingerToKey(keyMap, fingerPositions, numpadInfo);
			analysis.distance[numpadInfo.fingerUsed] += KB.MODIFIERPENALTY;
			analysis.fingerUsage[numpadInfo.fingerUsed]++;
			analysis.rowUsage[keyMap[numpadInfo.keyIndex].row]++;
			analysis.keyData[numpadInfo.keyIndex].count++;
			analysis.numKeys++;
			analysis.modifierUse.numpad++;

			if (analysis.tmp.prevFingerUsed === numpadInfo.fingerUsed) {
				var drow = Math.abs(keyMap[numpadInfo.keyIndex].row - keyMap[analysis.tmp.prevKeyIndex].row) + 1;
				var dx = 0.25 * Math.pow(drow, 2);
				analysis.consecFingerRowJump[numpadInfo.fingerUsed] += dx;
			}
			analysis.tmp.prevFingerUsed = numpadInfo.fingerUsed;

			tmpHand = KB.finger.leftRightOrThumb(numpadInfo.fingerUsed);
			if (analysis.tmp.prevHandUsed === tmpHand) {
				if (analysis.tmp.prevKeyIndex !== numpadInfo.keyIndex) {
					analysis.consecHandPressIgnoreDups[tmpHand]++;
				}
				analysis.consecHandPress[tmpHand]++;
			}
			analysis.tmp.prevHandUsed = tmpHand;

			analysis.tmp.prevKeyIndex = numpadInfo.keyIndex;

		} else if (typeof numpadInfo.fingerUsed !== "undefined") {
			analysis.modifierUse.numpad++;
			analysis.fingerUsage[numpadInfo.fingerUsed]++;
			analysis.distance[numpadInfo.fingerUsed] += KB.MODIFIERPENALTY;
			analysis.numKeys++;
		}

		if (typeof shiftInfo.fingerUsed !== "undefined" && typeof altGrInfo.fingerUsed !== "undefined") {
			analysis.modifierUse.shiftAltGr++;
			analysis.distance[shiftInfo.fingerUsed] += KB.MODIFIERPENALTY * 2;
			analysis.distance[altGrInfo.fingerUsed] += KB.MODIFIERPENALTY * 2;
			analysis.numKeys++;
			analysis.numKeys++;
		}


		// handle the key that was typed

		analysis.fingerUsage[keyInfo.fingerUsed]++;
		analysis.distance[keyInfo.fingerUsed] += moveFingerToKey(keyMap, fingerPositions, keyInfo);
		analysis.rowUsage[keyMap[keyInfo.keyIndex].row]++;
		analysis.keyData[keyInfo.keyIndex].count++;
		analysis.numKeys++;

		// same finger
		if (analysis.tmp.prevFingerUsed === keyInfo.fingerUsed) {
			var drow = Math.abs(keyMap[keyInfo.keyIndex].row - keyMap[analysis.tmp.prevKeyIndex].row) + 1;
			var dx = 0.25 * Math.pow(drow, 2);
			analysis.consecFingerRowJump[keyInfo.fingerUsed] += dx;
		}

		tmpHand = KB.finger.leftRightOrThumb(keyInfo.fingerUsed);
		if (analysis.tmp.prevHandUsed === tmpHand) {
			if (analysis.tmp.prevKeyIndex !== keyInfo.keyIndex) {
				// same hand, no dups
				analysis.consecHandPressIgnoreDups[tmpHand]++;

				// rolls
				analysis.rolls[keyInfo.fingerUsed]++;
				analysis.rolls[analysis.tmp.prevFingerUsed]++;
				// is roll on different rows?
				var curRow = keyMap[keyInfo.keyIndex].row;
				var prevRow = keyMap[analysis.tmp.prevKeyIndex].row;
				var curHomeRow = keyMap[keySet.fingerStart[keyInfo.fingerUsed]].row;
				var prevHomeRow = keyMap[keySet.fingerStart[analysis.tmp.prevFingerUsed]].row;

				// fingers move in same direction?
				if ( (curRow == curHomeRow) && (prevRow == prevHomeRow) ) {
					// both home rows
				}
				else if (curRow == prevRow) {
					// both same rows
				}
				else if ( (curRow == curHomeRow) || (prevRow == prevHomeRow)) {
					// one at home; other not
					if (curRow != curHomeRow) {
						analysis.rolls[keyInfo.fingerUsed]++;
					}
					if (prevRow != prevHomeRow) {
						analysis.rolls[analysis.tmp.prevFingerUsed]++;
					}
				}
				else if ( ((curRow - curHomeRow) < 0 && (prevRow - prevHomeRow) < 0) || ((curRow - curHomeRow) > 0 && (prevRow - prevHomeRow) > 0) ) {
					// fingers move in same direction
					if (curRow != curHomeRow) {
						analysis.rolls[keyInfo.fingerUsed] += Math.abs(curRow - curHomeRow);
					}
					if (prevRow != prevHomeRow) {
						analysis.rolls[analysis.tmp.prevFingerUsed] += Math.abs(prevRow - prevHomeRow);
					}
				}
				else {
					// fingers move in opposite direction
					if (curRow != curHomeRow) {
						analysis.rolls[keyInfo.fingerUsed] += Math.abs(curRow - curHomeRow) * 2;
					}
					if (prevRow != prevHomeRow) {
						analysis.rolls[analysis.tmp.prevFingerUsed] += Math.abs(prevRow - prevHomeRow) * 2;
					}
				}

				// need to extend finger sideways?
				if (keyInfo.keyIndex != keySet.fingerStart[keyInfo.fingerUsed]) {
					var penalty = Math.abs(keyMap[keyInfo.keyIndex].x - keyMap[keySet.fingerStart[keyInfo.fingerUsed]].x) / keyMap.normKeySize * 1.5;
					analysis.rolls[keyInfo.fingerUsed] += penalty;
				}
				if (analysis.tmp.prevKeyIndex != keySet.fingerStart[analysis.tmp.prevFingerUsed]) {
					var penalty = Math.abs(keyMap[analysis.tmp.prevKeyIndex].x - keyMap[keySet.fingerStart[analysis.tmp.prevFingerUsed]].x) / keyMap.normKeySize * 1.5;
					analysis.rolls[analysis.tmp.prevFingerUsed] += penalty;
				}

				// is roll (inward || outward)
				if ( (tmpHand == "left" && keyInfo.fingerUsed < analysis.tmp.prevFingerUsed) || (tmpHand == "right" && keyInfo.fingerUsed > analysis.tmp.prevFingerUsed) )  {
					// outward roll
					// extra effort
					analysis.rolls[keyInfo.fingerUsed]++;
					analysis.rolls[analysis.tmp.prevFingerUsed]++;

					analysis.rollsInout['out']++;
					if (analysis.tmp.prevRollsInout == 'in') {
						analysis.rolls[keyInfo.fingerUsed]++;
						analysis.rolls[analysis.tmp.prevFingerUsed]++;
					}
					analysis.tmp.prevRollsInout = 'out';
				}
				else {
					// inward roll
					analysis.rollsInout['in']++;
					if (analysis.tmp.prevRollsInout == 'out') {
						analysis.rolls[keyInfo.fingerUsed]++;
						analysis.rolls[analysis.tmp.prevFingerUsed]++;
					}
					analysis.tmp.prevRollsInout = 'in';
				}


			}

			// same hand, including dups
			analysis.consecHandPress[tmpHand]++;
		}

		// update key info for next press
		analysis.tmp.prevKeyIndex = keyInfo.keyIndex;
		analysis.tmp.prevHandUsed = tmpHand;
		analysis.tmp.prevShiftInfo = shiftInfo;
		analysis.tmp.prevAltGrInfo = altGrInfo;
		analysis.tmp.prevNumpadInfo = numpadInfo;
		analysis.tmp.prevFingerUsed = keyInfo.fingerUsed;

		//console.log("Leaving typeKey");
		return errors;
	}

	/*
	Updates the fingerPositions object and returns the distance the finger moved
	*/
	function moveFingerToKey(keyMap, fingerPositions, keyInfo) {
		//console.log("Entering moveFingerToKey");
		var dist = distanceBetweenKeysCached(keyMap, keyInfo.keyIndex, fingerPositions[keyInfo.fingerUsed]);
		fingerPositions[keyInfo.fingerUsed] = keyInfo.keyIndex;

		// base distance for pressing key
		dist += KB.PRESSDISTANCE;

		//console.log("Exiting moveFingerToKey");
		return dist;
	}

	/*
	config.keyMap
	config.keySet
	config.text
	*/
	me.examine = function (config) {
		if (!config || !config.keyMap || !config.keySet || typeof config.text === "undefined") {
			console.log("config object for examine function does not contain the needed parameters.");
			return;
		}

		var ii,
			jj,
			text = config.text.replace(/\r\n/g, "\r").replace(/\n/g, "\r"),
			tLen = text.length,
			keyMap = config.keyMap,
			keySet = config.keySet,
			charCode,
			finger,
			fingerLabel,
			curFingerPos = {},
			char2KeyMap = {},
			analysis = {}; // holds data we collect

		//console.log('-----')

		analysis.layoutName = keySet.label;
		analysis.keyboardType = keySet.keyboardType;
		analysis.pixelsPerCm = keyMap.pixelsPerCm;

		analysis.distance = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
		analysis.fingerUsage = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
		analysis.balance = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
		analysis.consecFingerRowJump = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
		analysis.rolls = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
		analysis.rollsInout = {
			"in": 0,
			"out": 0
		};
		analysis.consecHandPress = {
			"left": 0,
			"right": 0
		};
		analysis.consecHandPressIgnoreDups = {
			"left": 0,
			"right": 0
		};
		analysis.modifierUse = {
			"shift": 0,
			"altGr": 0,
			"shiftAltGr": 0,
			"numpad": 0
		};
		analysis.rowUsage = [0, 0, 0, 0, 0];
		analysis.errors = [];
		analysis.keyData = {
			length: 0
		}; // records number of times pushed
		analysis.charData = {}; // records information about characters // TODO: charData should only be computed once, not 5-6 times
		analysis.tmp = {}; // holds temporary data
		analysis.tmp.prevShiftInfo = {};
		analysis.tmp.prevAltGrInfo = {};
		analysis.tmp.prevNumpadInfo = {};
//		analysis.homeKeyWords = { "words": 0 };
//		analysis.homeBlockWords = { "words": 0, "score": 0 };

		analysis.numKeys = 0;
		analysis.tLen = tLen;

		// initialize data keyData and charData data set
		for (ii = 0; ii < keySet.keys.length; ii++) {
			analysis.keyData[ii] = {};
			analysis.keyData[ii].count = 0;
			analysis.keyData[ii].index = ii;
			analysis.keyData.length++;

			for (jj in KB.PUSH_TYPES) {
				if (KB.PUSH_TYPES.hasOwnProperty(jj)) {
					charCode = keySet.keys[ii][KB.PUSH_TYPES[jj]];
					if (typeof charCode === "number") {
						analysis.charData[charCode] = {};
					}
				}
			}
		}

		distanceBetweenKeysCached = memoize(distanceBetweenKeys);

		// initialize current finger positions
		curFingerPos[KB.finger.LEFT_PINKY] = keySet.fingerStart[KB.finger.LEFT_PINKY];
		curFingerPos[KB.finger.LEFT_RING] = keySet.fingerStart[KB.finger.LEFT_RING];
		curFingerPos[KB.finger.LEFT_MIDDLE] = keySet.fingerStart[KB.finger.LEFT_MIDDLE];
		curFingerPos[KB.finger.LEFT_INDEX] = keySet.fingerStart[KB.finger.LEFT_INDEX];
		curFingerPos[KB.finger.LEFT_THUMB] = keySet.fingerStart[KB.finger.LEFT_THUMB];
		curFingerPos[KB.finger.RIGHT_THUMB] = keySet.fingerStart[KB.finger.RIGHT_THUMB];
		curFingerPos[KB.finger.RIGHT_INDEX] = keySet.fingerStart[KB.finger.RIGHT_INDEX];
		curFingerPos[KB.finger.RIGHT_MIDDLE] = keySet.fingerStart[KB.finger.RIGHT_MIDDLE];
		curFingerPos[KB.finger.RIGHT_RING] = keySet.fingerStart[KB.finger.RIGHT_RING];
		curFingerPos[KB.finger.RIGHT_PINKY] = keySet.fingerStart[KB.finger.RIGHT_PINKY];

		// modifier keys
		char2KeyMap[16] = char2KeyMap[16] || findCharInKeySet(keySet, 16);
		char2KeyMap[-16] = char2KeyMap[-16] || findCharInKeySet(keySet, -16);
		char2KeyMap[-18] = char2KeyMap[-18] || findCharInKeySet(keySet, -18);
		char2KeyMap[19] = char2KeyMap[19] || findCharInKeySet(keySet, 19);

		if (char2KeyMap[16].errors.length > 1) {
			analysis.errors.push("Fatal Error: (left) Shift key not set correctly.");
			return analysis;
		}
		if (char2KeyMap[-16].errors.length > 1) {
			analysis.errors.push("Fatal Error: (right) Shift key not set correctly.");
			return analysis;
		}
		if (char2KeyMap[-18].errors.length > 1) {
			analysis.errors.push("Fatal Error: AltGr key not set correctly.");
			return analysis;
		}
		if (char2KeyMap[19].errors.length > 1) {
			analysis.errors.push("Fatal Error: Numpad key not set correctly.");
			return analysis;
		}

		for (ii = 0; ii < tLen; ii++) {
			charCode = text.charCodeAt(ii);
			//console.log(charCode);

			// return object contains: fingerUsed, keyIndex, pushType, errors
			char2KeyMap[charCode] = char2KeyMap[charCode] || findCharInKeySet(keySet, charCode);

			if (char2KeyMap[charCode].fingerUsed === null) {
				console.log(analysis.layoutName + ": Char code not found on keyboard (ignoring key): 0x " + charCode.toString(16).toUpperCase() + " (" + String.fromCharCode(charCode) + ")");
				analysis.errors.push.apply(analysis.errors, char2KeyMap[charCode].errors);
				continue;
			}

			returnFingersToHomeRow(keyMap, keySet.fingerStart, curFingerPos, analysis, getFingersUsed(char2KeyMap, char2KeyMap[charCode]));

			typeKey({
				keyInfo: char2KeyMap[charCode],
				char2KeyMap: char2KeyMap,
				fingerPositions: curFingerPos,
				keyMap: keyMap,
				keySet: keySet,
				analysis: analysis
			});
		}

		// done typing, but return fingers to the home row
		returnFingersToHomeRow(keyMap, keySet.fingerStart, curFingerPos, analysis);

		/*
		for (finger in KB.fingers) {
			if (KB.fingers.hasOwnProperty(finger)) {
				fingerLabel = KB.fingers[finger];

				console.log(fingerLabel + " distance:" + analysis.distance[ finger ]);
				var numCm = (analysis.distance[finger] / keyMap.pixelsPerCm);
				var numMeters = (numCm * 0.001);
				var numFeet = numMeters * 3.2808399;
				var numMiles = numMeters * 0.000621371192;

				console.log("meters:" + numMeters);
				console.log("feet:" + numFeet);
				console.log("miles:" + numMiles);
			}
		}
		//*/

		distanceBetweenKeysCached = null;

		analysis.homeKeyWords = countHomeKeyWords(keySet, text);
//		console.log("analysis.homeKeyWords.words: " + analysis.homeKeyWords.words);

		analysis.homeBlockWords = countHomeBlockWords(keySet, KB.keyMap[analysis.keyboardType].s683_225, text);

		analysis.singleHandWords = countSingleHandWords(keySet, KB.keyMap[analysis.keyboardType].s683_225, text);

		return analysis;
	};

	function countHomeKeyWords (keySet, text) {
		// count words typed by home keys
		// vars to use: text, keySet, keySet.fingerStart[finger]

		// retrieve home keys
		var homeKeys = Object.keys(keySet.fingerStart);

		// make sure modifiers also on home key
		var modAtHome = {
			shift: false,
			altGr: false,
			shiftAltGr: false,
			numpad: false,
		};

		homeKeys.map(
			function(cur) {
				var key = keySet.keys[keySet.fingerStart[cur]];

				if (key && key.primary) {
					var ch = key.primary;

					if (ch == KB.Key.LEFT_SHIFT || ch == KB.Key.RIGHT_SHIFT) { modAtHome.shift = true; };
					if (ch == KB.Key.ALT_GR) { modAtHome.altGr = true; };
					if (ch == KB.Key.NUM_LOCK) { modAtHome.numpad = true; };
				}
			}
		);

		modAtHome.shiftAltGr = modAtHome.shift && modAtHome.altGr;

		// create regexp pattern of only home letters
		var homePattern = '';
		homePattern = homeKeys.reduce(
			function(acc, cur, index, arr) {
				var letters = '';
				if (keySet.fingerStart[cur] > 0) { // finger is valid, not both thumbs

					var key = keySet.keys[keySet.fingerStart[cur]];

					letters += (key.primary > 0) ? String.fromCharCode(key.primary) : '';
					letters += (key.shift > 0 && modAtHome.shift) ? String.fromCharCode(key.shift) : '';
					letters += (key.altGr > 0 && modAtHome.altGr) ? String.fromCharCode(key.altGr) : '';
					letters += (key.shiftAltGr > 0 && modAtHome.shiftAltGr) ? String.fromCharCode(key.shiftAltGr) : '';
					letters += (key.numpad > 0 && modAtHome.numpad) ? String.fromCharCode(key.numpad) : '';
				}

				return acc + letters;
			},
			''
		);

		// clean up homePattern
		// 1. keep only basic letters
		// 2. omit or escape regexp special letters

		homePattern = homePattern.replace(/[^A-Za-z]/gm, '');
		//console.log("homePattern: " + homePattern);

//		var re = new RegExp("\\b[" + homePattern + "]{3,}\\b", 'gm');
		var re = new RegExp("[" + homePattern + "]{1,}", 'gm');
		//console.log("re: " + re);

		var matches = text.match(re);
//		console.log("matches: " + (matches !== null ? matches.join() : '' ));

		var retval = { "words": 0, "score": 0};
		matches && matches.map(
			function(cur) {
				this.words++;
				this.score += cur.length;
			}
			, retval
		);

//		console.log("HomeKeyWords retval: ");
//		console.log(retval);

		return retval;
	};

	function getHomeBlockKeys(keySet, keyMap) {
		// retrieve home block keys

		var homeKeys = Object.keys(keySet.fingerStart);
		var blockKeys = [];
		var blockFingers = [
			KB.finger.LEFT_RING,
			KB.finger.LEFT_MIDDLE,
			KB.finger.LEFT_INDEX,
			KB.finger.RIGHT_INDEX,
			KB.finger.RIGHT_MIDDLE,
			KB.finger.RIGHT_RING,
		];
		var thumbFingers = [
			KB.finger.LEFT_THUMB,
			KB.finger.RIGHT_THUMB,
		];
		var maxDistance = 55;

		homeKeys.map(
			function(cur) {
				var keyId = keySet.fingerStart[cur];
				var key = keySet.keys[keyId];

				if ( key ) {
					var finger = key.finger;
					var foundTop = false, foundBottom = false;
					if ( blockFingers.lastIndexOf(finger) != -1 ) {
						this.push(keyId);

						// find closest keys hit by finger, one above, one below
						var homeRow = keyMap[keyId].row;
						for (var i=0; i<keySet.keys.length; i++) {
							if (keySet.keys[i].finger == finger) {
								if (keyMap[i].row == homeRow - 1) {
									if ( (!foundTop && Math.abs(keyMap[i].cx - keyMap[keyId].cx ) < maxDistance) || ( foundTop && Math.abs(keyMap[i].cx - keyMap[keyId].cx ) < Math.abs(keyMap[foundTop].cx - keyMap[keyId].cx )) ) {
										// top row key found
										foundTop = i;
										continue;
									}
								}

								if (keyMap[i].row == homeRow + 1) {
									if (!foundBottom && Math.abs(keyMap[i].cx - keyMap[keyId].cx ) < maxDistance || ( foundBottom && Math.abs(keyMap[i].cx - keyMap[keyId].cx ) < Math.abs(keyMap[foundBottom].cx - keyMap[keyId].cx )) ) {
										// bottom row key found
										foundBottom = i;
										continue;
									}
								}
							}
						}
						if (foundTop) { this.push(foundTop);}
						if (foundBottom) { this.push(foundBottom);}

					} else if ( thumbFingers.lastIndexOf(finger) != -1 ) {
						this.push(keyId);
					}
				}

			}
			, blockKeys
		);

		return blockKeys;
	}

	function countHomeBlockWords(keySet, keyMap, text) {
		var homeKeys = Object.keys(keySet.fingerStart);
		var blockKeys = getHomeBlockKeys(keySet, keyMap);

/*/ debug
		console.log('');
		console.log("blockKeys:");
		console.log(blockKeys);
//*/

		// make sure modifiers also on home key
		var modAtHome = {
			shift: false,
			altGr: false,
			shiftAltGr: false,
			numpad: false,
		};

		homeKeys.map(
			function(cur) {
				var key = keySet.keys[keySet.fingerStart[cur]];

				if (key && key.primary) {
					var ch = key.primary;

					if (ch == KB.Key.LEFT_SHIFT || ch == KB.Key.RIGHT_SHIFT) { modAtHome.shift = true; };
					if (ch == KB.Key.ALT_GR) { modAtHome.altGr = true; };
					if (ch == KB.Key.NUM_LOCK) { modAtHome.numpad = true; };
				}
			}
		);

		modAtHome.shiftAltGr = modAtHome.shift && modAtHome.altGr;

		// create regexp pattern of only home letters
		var pattern = blockKeys.reduce(
			function(acc, cur) {
				var letters = '';
				var key = keySet.keys[cur];

				letters += (key.primary > 0) ? String.fromCharCode(key.primary) : '';
				letters += (key.shift > 0 && modAtHome.shift) ? String.fromCharCode(key.shift) : '';
				letters += (key.altGr > 0 && modAtHome.altGr) ? String.fromCharCode(key.altGr) : '';
				letters += (key.shiftAltGr > 0 && modAtHome.shiftAltGr) ? String.fromCharCode(key.shiftAltGr) : '';
				letters += (key.numpad > 0 && modAtHome.numpad) ? String.fromCharCode(key.numpad) : '';

				return acc + letters;
			}, ''
		);

		pattern = pattern.replace(/[^A-Za-z]/gm, '');
//		console.log("pattern: " + pattern);

//		var re = new RegExp("\\b[" + pattern + "]{3,}\\b", 'gm');
		var re = new RegExp("[" + pattern + "]{1,}", 'gm');
//		console.log("re: " + re);

		var matches = text.match(re);
//		console.log("matches: " + (matches !== null ? matches.join() : '' ));

		var retval = { "words": 0, "score": 0};
		matches && matches.map(
			function(cur) {
				this.words++;
				this.score += cur.length;
			}
			, retval
		);

//		console.log("HomeBlockWords retval: ");
//		console.log(retval);

		return retval;
	}

	function countSingleHandWords(keySet, keyMap, text) {
		var retval = { "words": 0, "score": 0};
		var hands = [ "", "" ];

		// find which finger has which letters
		// uppercase letters
		for (var code=65; code<=90; code++) {
			var f = findCharInKeySet(keySet, code).fingerUsed;
//console.log("finger: " + f);
			if (f) {
				if (KB.hands[KB.hand.LEFT_HAND].indexOf(f) != -1) {
					hands[KB.hand.LEFT_HAND] += String.fromCharCode(code);
				} else if (KB.hands[KB.hand.RIGHT_HAND].indexOf(f) != -1) {
					hands[KB.hand.RIGHT_HAND] += String.fromCharCode(code);
				}
			}
		}
		// lowercase letters
		for (var code=97; code<=122; code++) {
			var f = findCharInKeySet(keySet, code).fingerUsed;
//console.log("finger: " + f);
			if (f) {
				if (KB.hands[KB.hand.LEFT_HAND].indexOf(f) != -1) {
					hands[KB.hand.LEFT_HAND] += String.fromCharCode(code);
				} else if (KB.hands[KB.hand.RIGHT_HAND].indexOf(f) != -1) {
					hands[KB.hand.RIGHT_HAND] += String.fromCharCode(code);
				}
			}
		}

//console.log("left hand: " + hands[KB.hand.LEFT_HAND]);
//console.log("right hand: " + hands[KB.hand.RIGHT_HAND]);

		hands.map(
			function(cur, index) {
//				var re = new RegExp("\\b[" + cur + "]{3,}\\b", 'gm');
				var re = new RegExp("[" + cur + "]{3,}", 'gm');
//		console.log("re: " + re);

				var matches = text.match(re);
//		console.log("matches: " + (matches !== null ? matches.join() : '' ));
				matches && matches.map(
					function(cur) {
						retval.words++;
						retval.score += cur.length * KB.hScoring[index];
					}
				);
			}
		);

//		console.log("SingleHandWords retval: ");
//		console.log(retval);

		return retval;
	}

	function getMaxWords(text) {
		var matches = text.match(/\b[a-zA-Z]{3,}\b/gm);
		var words = matches && matches.reduce(
			function(acc, cur) {
				return acc + cur.length;
			}, 0
		);

		return words;
	}

	/*
	Takes in results from multiple calls to examine and scores them
	*/

	me.scoreLayouts = function (analysis, text) {
//		console.log("text: " + text);

		var results = {};

		results.ScoreClass = [
			{
				category: "Distance",
				enum: 0,
				weight: 18,
				penalty: 0.001121,
				func: Score.distance
			},
			{
				category: "Finger Usage",
				enum: 1,
				weight: 18,
				penalty: 0.048535,
				func: Score.fingerUsage
			},
			{
				category: "Rolls",
				enum: 2,
				weight: 18,
				penalty: 0.005488,
				func: Score.rolls
			},
			{
				category: "Consecutive Finger",
				enum: 3,
				weight: 18,
				penalty: 3.395,
				func: Score.sameFinger
			},
			{
				category: "Consecutive Hand",
				enum: 4,
				weight: 12,
				penalty: 0.39875,
				func: Score.sameHand
			},
			{
				category: "Balance",
				enum: 5,
				weight: 6,
				penalty: 0.1093,
				func: Score.balance
			},
			{
				category: "Words",
				enum: 6,
				weight: 6,
				penalty: 0.85,
				func: Score.words,
				numWords: getMaxWords(text)
			},
		];

//console.log("numWords: " + results.ScoreClass[4].numWords);

		results.finalList = [];

		for (var ii = 0, len = analysis.length; ii < len; ii++) {
			results.finalList[ii] = {}; // each kb to be analyzed
			results.finalList[ii].layoutName = analysis[ii].layoutName; // kb name
			results.finalList[ii].keyboardType = analysis[ii].keyboardType; // kb type
			results.finalList[ii].homeKeyWords = analysis[ii].homeKeyWords; // home key words
			results.finalList[ii].homeBlockWords = analysis[ii].homeBlockWords; // home block words
			results.finalList[ii].singleHandWords = analysis[ii].singleHandWords; // single hand words

// console.log("results.finalList[ii].singleHandWords.words: " + results.finalList[ii].singleHandWords.words);

			// calculate scores for each type of scoring as outlined in ScoreClass
			results.finalList[ii].scores = results.ScoreClass.map(
				function (category) {
					return category.func(this, category);
				}, analysis[ii]
			);

			// put it all together!
			results.finalList[ii].score = results.finalList[ii].scores.reduce(
				function (prev, curr) {
					return prev + curr;
				}, 0);
		}

		results.finalList.sort(function (a, b) {
			return a.score - b.score; // lower score wins
		});

		// relative effort to best layout
		var par = 8.0;
		for (var ii = 0, len = analysis.length; ii < len; ii++) {
			results.finalList[ii].extraEffort = "+" + ((results.finalList[ii].score / results.finalList[0].score - 1) * 100).toFixed(0) + "%";
			results.finalList[ii].par = (((results.finalList[ii].score - par) / par) * 100);
			if (0 < results.finalList[ii].par) {
				results.finalList[ii].par = "+" + results.finalList[ii].par.toFixed(0) + "%";
			} else if (0 > results.finalList[ii].par) {
				results.finalList[ii].par = "(" + results.finalList[ii].par.toFixed(0) + "%)";
			} else {
				results.finalList[ii].par = "---";
			}
		}

		return results;
	};

	return me;
})();