// create series of keyboardeditor

var kbcontainer = document.getElementById("kb-config-container");
var numLayers = 4;
for (var i=0; i < DB.vars.numPresets; i++) {
	var kbe = document.createElement("keyboardeditor");
	kbe.setAttribute("name", i);
	kbe.setAttribute("current", "current");
	kbe.className = "ng-isolate-scope";
	var div = document.createElement("div");
	div.id = "kla-kb-container-" + i;
	div.className = "hide kb-keyboard-container";
	div.style.padding = "0px";
	div.style.height = "252px";
	div.style.width = "754px";
	for (var j=0; j < numLayers; j++) {
		var canvas = document.createElement("canvas");
		canvas.id = "kb_keyboard" + i + "_canvas_l" + j;
		canvas.className = "kb-keyboard-overlay";
		canvas.style.height = "252px";
		canvas.style.width = "754px";
		if (j==3) {
			canvas.style.cursor = "pointer";
		}
		div.appendChild(canvas);
	}
	kbe.appendChild(div);
	kbcontainer.appendChild(kbe);
}
