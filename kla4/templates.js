angular.module('kla').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('partials/about.htm',
    "<div>\n" +
    "    <div class=\"jumbotron subhead\">\n" +
    "        <h1>About This App</h1>\n" +
    "        <p class=\"lead\">A brief introduction by Patrick Gillespie<p>\n" +
    "    </div>\n" +
    "    <p>\n" +
    "        This application allows you to analyze and visualize the typing patterns you create when you use different keyboard layouts, such as the\n" +
    "        <a href=\"http://home.earthlink.net/~dcrehr/\">QWERTY</a>, <a href=\"http://www.theworldofstuff.com/dvorak/\">Dvorak</a>, and\n" +
    "        <a href=\"http://colemak.com/\">Colemak</a> layouts.\n" +
    "    </p>\n" +
    "    <p>\n" +
    "        If you have no idea what I'm talking about, the key layout on your keyboard isn't the only one that's out there, and not all \n" +
    "        keyboard layouts are created equal. Some are better for your wrists and allow you to type faster and with more comfort. \n" +
    "        Here are the three most popular keyboard layouts (which I also mentioned above):\n" +
    "    </p>\n" +
    "    <p>\n" +
    "        <div style=\"text-align: center;\">\n" +
    "            <img src=\"../img/qwerty.png\" alt=\"\" />\n" +
    "            <br/>  \n" +
    "            <strong>QWERTY</strong>\n" +
    "        </div>\n" +
    "    </p>\n" +
    "    <p>\n" +
    "        <div style=\"text-align: center;\">\n" +
    "            <img src=\"../img/dvorak.png\" alt=\"\" />\n" +
    "            <br/>\n" +
    "            <strong>Dvorak (Simplified)</strong>\n" +
    "        </div>\n" +
    "    </p>\n" +
    "    <p>\n" +
    "        <div style=\"text-align: center;\">\n" +
    "            <img src=\"../img/colemak.png\" alt=\"\" />\n" +
    "            <br/>\n" +
    "            <strong>Colemak</strong>\n" +
    "        </div>\n" +
    "    </p>\n" +
    "    <p>\n" +
    "        My interest in keyboard layouts came after I read a Discover magazine article entitled <a href=\"http://discovermagazine.com/1997/apr/thecurseofqwerty1099\">\"The Curse of QWERTY\"</a>. The article tells the story of the QWERTY and Dvorak keyboard layouts and makes a compelling case for switching from a QWERTY layout to a Dvorak layout. Here is a quick summary of its most important points:\n" +
    "    </p>\n" +
    "    <p>\n" +
    "        <ul>\n" +
    "            <li>The QWERTY layout was created in the early 1870's before touch typing and without speed or comfort in mind.</li>\n" +
    "            <li>The Dvorak layout was created in the 1930's and is based on years of research. It takes speed and comfort into account.</li>\n" +
    "            <li>On average, the left hand does 56% of the typing when a QWERTY layout is used. With a Dvorak layout, the right hand does 56% of the typing.</li>\n" +
    "            <li>The Dvorak layout forces you to alternate hands more frequently when typing, this causes you to type faster.</li>\n" +
    "            <li>Users type fastest on the home row. With a QWERTY layout, only 32% of your typing occurs on the home row. With a Dvorak layout, 70% of your typing occurs on the home row.</li>\n" +
    "            <li>It's hypothesized that the Dvorak layout will make it less likely that you'll develop <a href=\"http://en.wikipedia.org/wiki/Carpal_tunnel_syndrome\">Carpal Tunnel Syndrome (CTS)</a>.</li>\n" +
    "            <li>Anecdotally, people who develop carpal tunnel syndrome seem to find relief when they switch from a QWERTY layout to Dvorak layout.</li>\n" +
    "        </ul>\n" +
    "    </p>\n" +
    "    <p>\n" +
    "        There are more reasons, but those were the ones that stuck with me. I was so convinced by what I read that I switched my work and home keyboard layouts to a Dvorak layout by configuring some Windows XP settings in the control panel (to see how <a href=\"http://kb.iu.edu/data/aepk.html\">click here</a>). This lasted for about 6 days (3 of those were over the Labor Day weekend), and then I had to switch back since learning the Dvorak layout was slowing me down at work. I also discovered that the Dvorak layout made all the nice QWERTY keyboard shortcuts (Undo, Cut, Copy and Paste) virtually unusable. This was a big minus since I use those shortcuts constantly. The Dvorak layout also over worked my right pinky. I found myself having to take typing breaks, something I hadn't done since high school.\n" +
    "    </p>\n" +
    "    <p>\n" +
    "        After talking to someone who had Carpal Tunnel Syndrome (something I'm worried about getting), I learned about yet another improved keyboard \n" +
    "        layout that preserved QWERTY's bottom row short cuts and didn't put massive amounts of stress on the right pinky. This layout was known as \n" +
    "        the Colemak. Unlike the Dvorak, the Colemak layout is relatively new (developed within the last 5 years), doesn't have a lot of research \n" +
    "        behind it, and it doesn't have a very large following (online estimates put the number of users between 650 and 1,300).  \n" +
    "        However, the layout looks pretty promising.\n" +
    "    </p>\n" +
    "    <p>\n" +
    "        All of this research is what motivated me to create this application. I wanted to visually compare my typing patterns with the different \n" +
    "        layouts and get some stats on what hand and which fingers I was using the most.\n" +
    "    </p>\n" +
    "    <p>\n" +
    "        Hopefully the user interface and output is straight forward enough. I had a lot of fun writing this application, hopefully some of you out \n" +
    "        there will find it useful.\n" +
    "    </p>\n" +
    "    <h3>Copyright</h3>\n" +
    "    <p>\n" +
    "        All images used and generated by this app are released under <a href=\"http://creativecommons.org/licenses/by/4.0/\">Creative Commons Attribution 4.0 license</a>. In short: feel free to use the images generated by this app however you want. I honestly don't care that much about attribution, so a footnote or mention somewhere in the document they're used in is fine.\n" +
    "    </p>\n" +
    "    <h3>Older Versions of the App</h3>\n" +
    "    <p>Patrick Gillespie's original app\n" +
    "        <ul>\n" +
    "            <li>\n" +
    "                <a href='http://patorjk.com/keyboard-layout-analyzer/v1/'>Version 1.0</a> - The original version created in 2008.\n" +
    "            </li>\n" +
    "            <li>\n" +
    "                <a href='http://patorjk.com/keyboard-layout-analyzer/v2/'>Version 2.0</a> - An overhauled re-write in 2012.\n" +
    "            </li>\n" +
    "            <li>\n" +
    "                <a href='http://patorjk.com/keyboard-layout-analyzer/'>Version 3.0</a> - The current version on <a href='http://patorjk.com/'>Patrick Gillespie's site</a>. Kept the core analysis code from 2012, and the code for the keyboard UI elements. The rest of the app was redesigned though, and the front-end code was re-architecuted to work with AngularJS, plus several features were added.\n" +
    "            </li>\n" +
    "        </ul>\n" +
    "    </p>\n" +
    "    <p>Shena'Fu's modifications\n" +
    "        <ul>\n" +
    "            <li>\n" +
    "                <a href='/Keyboard Layout Analyzer.html'>Version 1</a> - Scoring now normalized on total effort at unidirectional scaling. Different penalties for each finger. Added Ergolinear and Matrix keyboard designs.\n" +
    "            </li>\n" +
    "            <li>\n" +
    "                <a href='/Keyboard Layout Analyzer 2.html'>Version 2</a> - Summaries and graphs expanded to show more details. Slightly modified penalty parameters.\n" +
    "            </li>\n" +
    "            <li>\n" +
    "                <a href='/Keyboard Layout Analyzer 3.html'>Version 3</a> - Current version. Work in progress. Rip out all extraneous code and bloat (i.e. social media crud). Fixed graphical bugs in version 2. Edited the About page.\n" +
    "            </li>\n" +
    "            <li>\n" +
    "                TODO - Will add more typing calculations (e.g. rolls, seesaw). Allow user to configure penalties and other parameters.\n" +
    "            </li>\n" +
    "        </ul>\n" +
    "    </p>\n" +
    "</div>"
  );


  $templateCache.put('partials/layouts.htm',
    "  <div>\n" +
    "        <div class=\"jumbotron subhead\">\n" +
    "            <h1>Keyboard Layouts</h1>\n" +
    "        </div>\n" +
    "    <div id='kb-config-container'>\n" +
    "        <h3>Currently loaded layouts:</h3>\n" +
    "        <paginate start=\"1\" stop=\"{{keyboards.getLayouts().length}}\" handler=\"switchLayout\"></paginate>\n" +
    "    \n" +
    "            <p class=\"lead\"><strong>Click</strong> or <strong>Drag</strong> the keys on the keyboard below to modify them<p>\n" +
    "        <div ng-repeat=\"k in keyboards.getLayouts()\"><keyboardeditor name=\"{{$index}}\" current=\"current\"></keyboardeditor></div>\n" +
    "        <table class=\"kb-config-table\">\n" +
    "            <thead>\n" +
    "                <th>\n" +
    "                    Properties\n" +
    "                </th>\n" +
    "                <th>\n" +
    "                    Data\n" +
    "                </th>\n" +
    "            </thead>\n" +
    "            <tbody>\n" +
    "                <tr>\n" +
    "                    <td class=\"kb-config-td\">\n" +
    "                        <div class=\"kb-config-editor\">\n" +
    "                            <form class='form-horizontal'>\n" +
    "    \n" +
    "                                <div class='control-group'>\n" +
    "                                    <label class='control-label' for=\"kb-config-name\">Layout Name:</label>\n" +
    "                                    <div class='controls'>\n" +
    "                                        <input id=\"kb-config-name\" class=\"kb-config-name\" type=\"text\" ng-model=\"keyboards.getLayout(current).keySet.label\"/>\n" +
    "                                    </div>\n" +
    "                                </div>\n" +
    "    \n" +
    "                                <div class='control-group'>\n" +
    "                                    <label class='control-label' for=\"kb-config-kbtype\">Form Factor:</label>\n" +
    "                                    <div class='controls'>\n" +
    "                                        <div style='padding:5px 7px;' ng-if=\"keyboards.getLayout(current).keySet.keyboardType\">\n" +
    "                                            {{keyboards.getLayout(current).keySet.keyboardType}}\n" +
    "                                        </div>\n" +
    "                                    </div>\n" +
    "                                </div>\n" +
    "    \n" +
    "                                <div class='control-group'>\n" +
    "                                    <label class='control-label' for=\"kb-config-kbtype\">Submitted By:</label>\n" +
    "                                    <div class='controls'>\n" +
    "                                        <div style='padding:5px 7px;' ng-if=\"keyboards.getLayout(current).keySet.authorUrl\">\n" +
    "                                            <a href='{{keyboards.getLayout(current).keySet.authorUrl}}'>{{keyboards.getLayout(current).keySet.author}}</a>\n" +
    "                                        </div>\n" +
    "\n" +
    "                                        <div style='padding:5px 7px;' ng-if=\"!keyboards.getLayout(current).keySet.authorUrl\">{{keyboards.getLayout(current).keySet.author}}</div>\n" +
    "                                    </div>\n" +
    "                                </div>\n" +
    "    \n" +
    "                                <div class='control-group'>\n" +
    "                                    <label class='control-label' for=\"kb-config-kbtype\">More Info:</label>\n" +
    "                                    <div class='controls'>\n" +
    "                                    <div ng-show='!isMoreInfo(keyboards.getLayout(current).keySet)' style='padding:5px 7px;'>None</div>\n" +
    "                                    <div ng-show='isMoreInfo(keyboards.getLayout(current).keySet)' style='padding:5px 7px;'>\n" +
    "                                    <a href='{{keyboards.getLayout(current).keySet.moreInfoUrl}}'>{{keyboards.getLayout(current).keySet.moreInfoText}}</a>\n" +
    "                                    </div>\n" +
    "                                    </div>\n" +
    "                                </div>\n" +
    "    \n" +
    "                            </form>\n" +
    "                        </div>\n" +
    "                    </td>\n" +
    "                    <td class=\"kb-config-td\">\n" +
    "    \n" +
    "                        <div class=\"kb-config-editor\">\n" +
    "                            <form class='form-horizontal'>\n" +
    "    \n" +
    "                                <div class='control-group'>\n" +
    "                                    <label class='control-label'>Import Layout:</label>\n" +
    "                                    <div class='controls'>\n" +
    "                                        <button class=\"kb-config-import btn\" ng-click=\"showImportDialog()\">Import</button>\n" +
    "                                    </div>\n" +
    "                                </div>\n" +
    "    \n" +
    "                                <div class='control-group'>\n" +
    "                                    <label class='control-label'>Export Layout:</label>\n" +
    "                                    <div class='controls'>\n" +
    "                                        <button class=\"kb-config-export btn\" ng-click=\"showExportDialog()\">to JSON</button>\n" +
    "                                        <button class=\"kb-config-export btn\" ng-click=\"showVsvDialog()\">to VSV</button>\n" +
    "                                    </div>\n" +
    "                                </div>\n" +
    "    \n" +
    "                                <div class='control-group'>\n" +
    "                                    <label class='control-label' for='kb-config-select-list'>Preset Layout:</label>\n" +
    "                                    <div class='controls'>\n" +
    "                                        <select id='kb-config-select-list' class='kb-config-select-list' ng-model='keyboards' ng-change='loadLayout()' ng-controller='LoadLayoutCtrl' ng-options='layout.file as layout.title group by layout.type for layout in layouts track by layout.file'>\n" +
    "                                            <option value=''>[Select Layout]</option>\n" +
    "                                        </select>\n" +
    "                                    </div>\n" +
    "                                </div>\n" +
    "    \n" +
    "                                <div class='control-group'>\n" +
    "                                    <label class='control-label'>Layout Image:</label>\n" +
    "                                    <div class='controls'>\n" +
    "                              	         <button class=\"kb-config-make-image btn\" ng-click=\"makeImage()\">Generate Image</button> => \n" +
    "                                        <a id='generated-image-link'><img id='generated-image' alt='' width='200'></a>" +
    "                                    </div>\n" +
    "                                </div>\n" +
    "    \n" +
    "                            </form>\n" +
    "                        </div>\n" +
    "    \n" +
    "                    </td>\n" +
    "                </tr>\n" +
    "            </tbody>\n" +
    "        </table>\n" +
    "    \n" +
    "        <!-- import modal -->\n" +
    "        <div id='kb-config-import-dialog' class='modal hide fade' tabindex='-1' role='dialog' aria-labelledby='resultLabel' aria-hidden='true'>\n" +
    "            <div class='modal-header'>Keyboard Layout Analyzer.html#summary\n" +
    "                <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>x</button>\n" +
    "                <h3 id='resultLabel'>Import Layout</h3>\n" +
    "            </div>\n" +
    "            <div class='modal-body'>\n" +
    "    \n" +
    "                <textarea class='input-block-level kb-config-dialog-txt'></textarea>\n" +
    "                <p class='text-left'>\n" +
    "                    Paste the text of a previously exported layout in the textbox above and press \"Import\" to load the layout.\n" +
    "                </p>\n" +
    "            </div>\n" +
    "    \n" +
    "            <div class='modal-footer'>\n" +
    "                <button class=\"btn\" ng-click=\"importLayout()\">Import</button>\n" +
    "                <button class=\"btn\" data-dismiss=\"modal\" aria-hidden=\"true\">Close</button>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    \n" +
    "        <!-- import modal -->\n" +
    "        <div id='kb-config-export-dialog' class='modal hide fade' tabindex='-1' role='dialog' aria-labelledby='resultLabel' aria-hidden='true'>\n" +
    "            <div class='modal-header'>\n" +
    "                <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>x</button>\n" +
    "                <h3 id='resultLabel'>Export Layout</h3>\n" +
    "            </div>\n" +
    "            <div class='modal-body'>\n" +
    "    \n" +
    "                <textarea class='input-block-level kb-config-dialog-txt'></textarea>\n" +
    "                <p class='text-left'>\n" +
    "                    The above text represents the keyboard layout. You can come back to the app later and load this layout with this text using the \"Import\" feature.\n" +
    "                </p>\n" +
    "            </div>\n" +
    "    \n" +
    "            <div class='modal-footer'>\n" +
	 "                <a id=\"layout-download-link\" class=\"btn\">Download to File</a> \n" +
    "                <button class=\"btn\" ng-click=\"selectAllExportText()\">Copy to Clipboard</button>\n" +
    "                <button class=\"btn\" data-dismiss=\"modal\" aria-hidden=\"true\">Close</button>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    \n" +
    "</div>\n"
  );


  $templateCache.put('partials/load.htm',
    "<div class='loading-container text-center'>\n" +
    "    <p>\n" +
    "    \t<img src='../img/qwerty.png'/>\n" +
    "    </p>\n" +
    "    <p>\n" +
    "    \t<img src='../img/loading2.gif'>\n" +
    "    </p>\n" +
    "    Loading, one moment please...\n" +
    "</div>"
  );


  $templateCache.put('partials/compare.htm',
    "<div style='height: 100%; position: relative;'>\n" +
    "    <div class=\"jumbotron subhead\">\n" +
    "        <h1>Compare and Analyze</h1>\n" +
    "        <p class=\"lead\">See which layout is best for your input text. Compare the layouts, then analyze their scores and data.<p>\n" +
    "    </div>\n" +
    "    <form class='form-vertical' id='text-input-form'>\n" +
    "        <div class='control-group'>\n" +
    "            <label class='control-label' for='txt-input'>Text to Analyze:</label>\n" +
    "            <div class='controls' id='text-input-area'>\n" +
    "                <textarea id='txt-input' class='input-block-level' ng-model='data.text'></textarea>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class='control-group' id='text-preset-control'>\n" +
    "            <label class='control-label' for='text-presets'>Text Presets:</label>\n" +
    "            <div class='controls'>\n" +
    "                <select id='text-presets' ng-model='data.textPreset' ng-change='applyPreset()' ng-controller='CorpusCtrl' ng-options='corpus.file as corpus.title group by corpus.type for corpus in corpora track by corpus.file' size='20' ng-dblclick=\"generateOutput(data.text)\" ng-keypress=\"onKeyPress(e);\">\n" +
    "                    <option value=''>[Select Text to Load]</option>\n" +
    "                </select>\n" +
    "                <button class=\"btn btn-large\" type=\"button\" ng-click=\"generateOutput(data.text)\">Compare Layouts</button>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </form>\n" +
    "</div>"
  );


  $templateCache.put('partials/paginate.htm',
    "<div class=\"pagination pagination\">\n" +
    "    <ul>\n" +
    "        <li class=\"switcher\" num=\"prev\" ng-click=\"handleNav($event, start*1,'prev')\">\n" +
    "            <a href=\"javascript:void(0);\" >←</a>\n" +
    "        </li>\n" +
    "\n" +
    "        <li ng-repeat='ii in [start, stop] | makeRange' \n" +
    "            ng-class=\"{switcher: true, active: ($index === current)}\" \n" +
    "            num=\"$index\" ng-click=\"handleNav($event, start*1, $index)\">\n" +
    "            <a href=\"javascript:void(0);\" >{{start*1+$index}}</a>\n" +
    "        </li>\n" +
    "\n" +
    "        <li class=\"switcher\" num=\"next\" ng-click=\"handleNav($event, start*1, 'next')\">\n" +
    "            <a href=\"javascript:void(0);\" >→</a>\n" +
    "        </li>\n" +
    "    </ul>\n" +
    "</div>"
  );


  $templateCache.put('partials/result-options.htm',
    "<div class='text-center kla-result-opts'>\n" +
    "    <!--<h4 class='text-center kla-display-opts-header'>Result Table Display Options</h4>-->\n" +
    "    <div class=\"btn-group text-left\">\n" +
    "        <button data-toggle=\"dropdown\" class=\"btn dropdown-toggle\"  data-placeholder=\"false\">Units: {{source.units}}<span class=\"caret\"></span></button>\n" +
    "        <ul class=\"dropdown-menu\">\n" +
    "            <li ng-repeat=\"curUnit in source.allowedUnits\">\n" +
    "                <input type=\"radio\" name='kla-opt{{settings.id}}-unit-radio' id=\"kla-opt{{settings.id}}-units-{{$index}}\" ng-model=\"source.units\" value='{{curUnit}}'><label for=\"kla-opt{{settings.id}}-units-{{$index}}\" >{{curUnit}}</label>\n" +
    "            </li>\n" +
    "        </ul>\n" +
    "    </div> \n" +
    "\n" +
    "    <div class=\"btn-group text-left\" style='display:{{settings.showDisplayType}}'>\n" +
    "        <button data-toggle=\"dropdown\" class=\"btn dropdown-toggle\"  data-placeholder=\"false\">Display: {{source.displayType}}<span class=\"caret\"></span></button>\n" +
    "        <ul class=\"dropdown-menu\">\n" +
    "            <li ng-repeat=\"(dType, dValue) in source.displayData track by $index\">\n" +
    "                <input type=\"radio\" name='kla-opt{{settings.id}}-unit-radio' id=\"kla-opt{{settings.id}}-d-{{$index}}\" ng-model=\"source.displayType\" value='{{dType}}'><label for=\"kla-opt{{settings.id}}-d-{{$index}}\" >{{dType}}</label>\n" +
    "            </li>\n" +
    "        </ul>\n" +
    "    </div> \n" +
    "\n" +
    "    <div class=\"btn-group text-left\">\n" +
    "        <button data-toggle=\"dropdown\" class=\"btn dropdown-toggle\"  data-placeholder=\"false\">Keyboards <span class=\"caret\"></span></button>\n" +
    "        <ul class=\"dropdown-menu\">\n" +
    "            <li ng-repeat=\"layout in source.seriesData.allSeriesLabels\">\n" +
    "                <input type=\"checkbox\" id=\"kla-opt{{settings.id}}-dd-{{$index}}\" ng-model=\"source.rawSeriesData[$index].visible\"><label for=\"kla-opt{{settings.id}}-dd-{{$index}}\" >{{layout}}</label>\n" +
    "            </li>\n" +
    "        </ul>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('partials/result-table.htm',
    "<table class='kla-table-data'>\n" +
    "    <thead>\n" +
    "        <tr>\n" +
    "            <th>\n" +
    "            </th>\n" +
    "            <th ng-repeat=\"header in source.displayData[source.displayType]\">\n" +
    "                <div class='text-right kla-table-data-text kla-padding-left'>{{header.label}}</div>\n" +
    "            </th>\n" +
    "            <th>\n" +
    "                <div class='text-right kla-table-data-text kla-padding-left'>Total</div>\n" +
    "            </th>\n" +
    "        </tr>\n" +
    "    </thead>\n" +
    "    <tbody>\n" +
    "        <tr ng-repeat=\"layout in source.seriesData.seriesLabels\"  ng-click=\"selectRow($event)\">\n" +
    "            <td><div class='text-right'>{{layout}}</div></td>\n" +
    "            <td class='kla-table-data-text' ng-repeat=\"dataPoint in source.seriesData[$index] track by $id($index)\">\n" +
    "                <div class='text-right'>{{format(dataPoint)}}</div>\n" +
    "            </td>\n" +
    "            <td class='kla-table-data-text'>\n" +
    "                <div class='text-right kla-padding-left-total'>{{format(source.seriesData[$index].total)}}</div>\n" +
    "            </td>\n" +
    "        </tr>\n" +
    "    </tbody>\n" +
    "</table>"
  );


  $templateCache.put('partials/results.htm',
    "<div>\n" +
    "    <div class=\"jumbotron subhead\">\n" +
    "        <h1>Results</h1>\n" +
    "        <p class=\"lead\">See each tab for detailed analysis<p>\n" +
    "    </div>\n" +
    "    <ul class='nav nav-pills' id='compare-output-tabs'>\n" +
    "        <li class='kla-pill active'><a ng-click='tabSwitch($event, \"summary\")' href='#summary'>Summary</a></li>\n" +
    "        <li class='kla-pill'><a ng-click='tabSwitch($event, \"distance\")' href='#distance'>Distance</a></li>\n" +
    "        <li class='kla-pill'><a ng-click='tabSwitch($event, \"fingerUsage\")' href='#fingerUsage'>Finger Usage</a></li>\n" +
    "        <li class='kla-pill'><a ng-click='tabSwitch($event, \"rolls\")' href='#rolls'>Rolls</a></li>\n" +
    "        <li class='kla-pill'><a ng-click='tabSwitch($event, \"consecutive\")' href='#consecutive'>Same Finger & Hand</a></li>\n" +
    "        <li class='kla-pill'><a ng-click='tabSwitch($event, \"balance\")' href='#balance'>Balance</a></li>\n" +
    "        <li class='kla-pill'><a ng-click='tabSwitch($event, \"words\")' href='#words'>Words</a></li>\n" +
    "        <li class='kla-pill'><a ng-click='tabSwitch($event, \"rowUsage\")' href='#rowUsage'>Row Usage</a></li>\n" +
    "        <li class='kla-pill'><a ng-click='tabSwitch($event, \"heatMaps\")' href='#heatMaps'>Heat Maps</a></li>\n" +
    "        <li class='kla-pill'><a ng-click='tabSwitch($event, \"miscellaneous\")' href='#miscellaneous'>Miscellaneous</a></li>\n" +
    "    </ul>\n" +
    "    <div class='tab-content'>\n" +
    "        <div class='tab-pane active' id='summary'>\n" +
    "            <div class='best-layout'>\n" +
    "                <h3>Best Layout Is:</h3>\n" +
    "                <p><img src='../img/trophy-32.png' style='display:inline-block; position:relative; top:-4px;margin-right:6px;'>\n" +
    "                {{results.summary.bestLayout}}</p>\n" +
    "            </div>\n" +
    "                <div class=\"kla-result-table\">\n" +
    "\n" +
    "                    <table class='kla-table-data kla-table-data'>\n" +
    "                        <thead>\n" +
    "                            <tr>\n" +
    "                                <th width=\"4em\"><div class='text-right'>Rank</div></th>\n" +
    "                                <th><div class='text-left'>Layout</div></th>\n" +
    "                                <th><div class='text-left'>Board</div></th>\n" +
    "                                <th><div class='text-right'>+Best</div></th>\n" +
    "                                <th><div class='text-right'>±Par</div></th>\n" +
    "                                <th><div class='text-right'>Overall Score</div></th>\n" +
    "                                <th><div class='text-right'>Distance</div></th>\n" +
    "                                <th><div class='text-right'>Finger Usage</div></th>\n" +
    "                                <th><div class='text-right'>Rolls</div></th>\n" +
    "                                <th><div class='text-right'>Same Finger</div></th>\n" +
    "                                <th><div class='text-right'>Same Hand</div></th>\n" +
    "                                <th><div class='text-right'>Balance</div></th>\n" +
    "                                <th><div class='text-right'>Words</div></th>\n" +
    "                            </tr>\n" +
    "                        </thead>\n" +
    "                        <tbody>\n" +
    "    \n" +
    "                            <tr ng-repeat=\"layout in results.summary.rankedLayouts\"  ng-click=\"selectRow($event)\" " +
    ">\n" +
    "                                <td><div class='text-right'>#{{$index + 1}}</div></td>\n" +
    "                                <td><div class='text-left'>{{layout.layoutName}}</div></td>\n" +
    "                                <td><div class='text-left'>{{layout.keyboardType}}</div></td>\n" +
    "                                <td><div class='text-right'>{{layout.extraEffort}}</div></td>\n" +
    "                                <td><div class='text-right'>{{layout.par}}</div></td>\n" +
    "                                <td><div class='text-right'>{{(layout.score * 10).toFixed(2)}}</div></td>\n" +
    "                                <td><div class='text-right'>{{(layout.scores[0] * 10).toFixed(2)}}</div></td>\n" +
    "                                <td><div class='text-right'>{{(layout.scores[1] * 10).toFixed(2)}}</div></td>\n" +
    "                                <td><div class='text-right'>{{(layout.scores[2] * 10).toFixed(2)}}</div></td>\n" +
    "                                <td><div class='text-right'>{{(layout.scores[3] * 10).toFixed(2)}}</div></td>\n" +
    "                                <td><div class='text-right'>{{(layout.scores[4] * 10).toFixed(2)}}</div></td>\n" +
    "                                <td><div class='text-right'>{{(layout.scores[5] * 10).toFixed(2)}}</div></td>\n" +
    "                                <td><div class='text-right'>{{(layout.scores[6] * 10).toFixed(2)}}</div></td>\n" +
    "                            </tr>\n" +
    "                    \n" +
    "                        </tbody>\n" +
    "                    </table>\n" +
    "\n" +
    "                </div>\n" +
    "            <p></p>\n" +
    "            <p class='kla-pie-label'>Scoring Efforts:</p>\n" +
    "            <p>\n" +
    "                The overall layout score is based on a weighed calculation that factors in: \n" +
    "                the total distance your fingers travelled (3/16); \n" +
    "                how often you use stronger fingers (3/16); \n" +
    "                the direction of finger rolls (3/16); \n" +
    "                how often you switch fingers (3/16) and hands (2/16) between key presses; \n" +
    "                balancing fingers and hands usage (1/16); \n" +
    "                and the effort to type whole words (1/16). \n" +
    "                Lower scores are better, means less effort will be used during typing.\n" +
    "                <br><br>Whereas the par overall score is 80.00, the optimal, more ergonomic, more efficient layouts will have lower efforts scores, and the worse, less ergonomic, less efficient layouts will have higher effort scores.\n" +
    "                By looking at the ±Par column, you can see how much more or less effort is demanded compared to the par.\n" +
    "                By looking at the +Best column, you can see how much more effort is demanded compared to the best layout on this page.\n" +
    "            </p>\n" +
    "            <p class='kla-pie-label'>Corpus Synopsis: </p><textarea cols='200' rows='3' style='width:100%'>{{results.synopsis}}</textarea>\n" +
    "        </div>\n" +
    "        <div class='tab-pane' id='distance' style='position:relative'>\n" +
    "            <h3>Distance</h3>\n" +
    "            <p>How much your fingers move around the keyboard. Lower scores are better. Distance accounts for 4/16 of the final score.</p>\n" +
    "            <seriesbarchart width=\"100%\" height=\"350px\" source=\"results.distance\"></seriesbarchart>\n" +
    "            <resulttable source='results.distance'></resulttable>\n" +
    "            <resultoptions source='results.distance'></resultoptions>\n" +
    "\n" +
    "            <div class='kla-piecharts'>\n" +
    "                <h4 class='text-center kla-pie-header'>Pie Chart Visualizations</h4>\n" +
    "\n" +
    "                <div ng-repeat=\"layout in results.distance.seriesData.seriesLabels track by $id($index)\" class='kla-pie-container'>\n" +
    "                    <div class='kla-pie-label'>{{layout}}</div>\n" +
    "                    <piechart width=\"500px\" height=\"330px\" source=\"results.distance\" series=\"$index\"></piechart>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class='tab-pane' id='rolls' style='position:relative'>\n" +
    "            <h3>Rolls: by Finger</h3>\n" +
    "            <p>When typing consecutive keys on the same hand, stronger and longer fingers are more desirable, faster, easier. Comparatively, weaker and shorter fingers will exert more effort to perform dexterity-testing finger operations like rolls. Rolls accounts for 3/16 of the final score.</p>\n" +
    "            <seriesbarchart width=\"100%\" height=\"350px\" source=\"results.rolls\"></seriesbarchart>\n" +
    "            <resulttable source='results.rolls'></resulttable>\n" +
    "            <resultoptions source='results.rolls'></resultoptions>\n" +
    "\n" +
    "            <div class='kla-piecharts'>\n" +
    "                <h4 class='text-center kla-pie-header'>Pie Chart Visualizations</h4>\n" +
    "\n" +
    "                <div ng-repeat=\"layout in results.rolls.seriesData.seriesLabels track by $id($index)\" class='kla-pie-container'>\n" +
    "                    <div class='kla-pie-label'>{{layout}}</div>\n" +
    "                    <piechart width=\"500px\" height=\"330px\" source=\"results.rolls\" series=\"$index\"></piechart>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <h3>Rolls: Inward & Outward</h3>\n" +
    "            <p>When typing consecutive keys on the same hand, finger rolling inward--from pinky to index--are faster and comfortable. Thus outward rolls cost more effort and less desirable.</p>\n" +
    "            <seriesbarchart width=\"100%\" height=\"350px\" source=\"results.rollsInout\"></seriesbarchart>\n" +
    "            <resulttable source='results.rollsInout'></resulttable>\n" +
    "            <resultoptions source='results.rollsInout' displayopts=false></resultoptions>\n" +
    "\n" +
    "            <div class='kla-piecharts'>\n" +
    "                <h4 class='text-center kla-pie-header'>Pie Chart Visualizations</h4>\n" +
    "\n" +
    "                <div ng-repeat=\"layout in results.rollsInout.seriesData.seriesLabels track by $id($index)\" class='kla-pie-container'>\n" +
    "                    <div class='kla-pie-label'>{{layout}}</div>\n" +
    "                    <piechart width=\"500px\" height=\"330px\" source=\"results.rollsInout\" series=\"$index\"></piechart>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class='tab-pane' id='fingerUsage'>\n" +
    "            <h3>Finger Usage</h3>\n" +
    "            <p>Your stronger fingers should do more typing than weaker fingers. Lower scores are better. Finger Usage accounts for 2/16 of the final score.</p>\n" +
    "            <seriesbarchart width=\"100%\" height=\"350px\" source=\"results.fingerUsage\"></seriesbarchart>\n" +
    "            <resulttable source='results.fingerUsage'></resulttable>\n" +
    "            <resultoptions source='results.fingerUsage'></resultoptions>\n" +
    "\n" +
    "            <div class='kla-piecharts'>\n" +
    "                <h4 class='text-center kla-display-opts-header'>Pie Chart Visualizations</h4>\n" +
    "\n" +
    "                <div ng-repeat=\"layout in results.fingerUsage.seriesData.seriesLabels track by $id($index)\" class='kla-pie-container'>\n" +
    "                    <div class='kla-pie-label'>{{layout}}</div>\n" +
    "                    <piechart width=\"500px\" height=\"330px\" source=\"results.fingerUsage\" series=\"$index\"></piechart>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class='tab-pane' id='balance' style='position:relative'>\n" +
    "            <h3>Balance</h3>\n" +
    "            <p>Balance is whether effort is distributed across fingers and hands to an ideal ratio according to finger and hand strength and length.  Finger usage above the ideal amount is counted as extra effort. Balance accounts for 1/16 of the final score.</p>\n" +
    "            <seriesbarchart width=\"100%\" height=\"350px\" source=\"results.balance\"></seriesbarchart>\n" +
    "            <resulttable source='results.balance'></resulttable>\n" +
    "            <resultoptions source='results.balance'></resultoptions>\n" +
    "\n" +
    "            <div class='kla-piecharts'>\n" +
    "                <h4 class='text-center kla-pie-header'>Pie Chart Visualizations</h4>\n" +
    "\n" +
    "                <div ng-repeat=\"layout in results.balance.seriesData.seriesLabels track by $id($index)\" class='kla-pie-container'>\n" +
    "                    <div class='kla-pie-label'>{{layout}}</div>\n" +
    "                    <piechart width=\"500px\" height=\"330px\" source=\"results.balance\" series=\"$index\"></piechart>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class='tab-pane' id='consecutive'>\n" +
    "            <div class=\"kla-misc-box\">\n" +
    "                <h3>Consecutive Finger Use</h3>\n" +
    "                <p>How often the same finger is used to type two keys consecutively, further penalized by the row difference of the keys (a.k.a row jumping). An example of this would be typing \"ed\" on QWERTY. When looking at \"d\", the program notes that the index finger was also previously used to type \"e\".</p>\n" +
    "                <p>Lower scores are better. Consecutive Finger Use accounts for 3/16 of the final score.</p>\n" +
    "            </div>\n" +
    "            <seriesbarchart width=\"100%\" height=\"350px\" source=\"results.consecFinger\"></seriesbarchart>\n" +
    "            <resulttable source='results.consecFinger'></resulttable>\n" +
    "            <resultoptions source='results.consecFinger'></resultoptions>\n" +
    "\n" +
    "            <div class='kla-piecharts'>\n" +
    "                <h4 class='text-center kla-pie-header'>Pie Chart Visualizations</h4>\n" +
    "\n" +
    "                <div ng-repeat=\"layout in results.consecFinger.seriesData.seriesLabels track by $id($index)\" class='kla-pie-container'>\n" +
    "                    <div class='kla-pie-label'>{{layout}}</div>\n" +
    "                    <piechart width=\"500px\" height=\"330px\" source=\"results.consecFinger\" series=\"$index\"></piechart>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"kla-misc-box\">\n" +
    "                <h3>Consecutive Hand and Thumb Use</h3>\n" +
    "                <p>How often the same hand was used to type two keys consecutively, including modifiers. An example of this would be typing \"af\" on QWERTY. When looking at \"f\", the program notes that the left hand was also previously used to type \"a\".</p>\n" +
   "                <p>Lower scores are better.  Consecutive Hand Use accounts for 2/16 of the final score.</p>\n" +
    "                <label><input class='kla-result-checkbox' ng-model=\"settings.chuIgnoreDups\" type=\"checkbox\" /> Include instances of the same key being pressed twice in a row (example: typing \"ff\").</label>\n" +
    "            </div>\n" +
    "            <seriesbarchart width=\"100%\" height=\"350px\" source=\"results.consecHandPress\"></seriesbarchart>\n" +
    "            <resulttable source='results.consecHandPress'></resulttable>\n" +
    "            <resultoptions source='results.consecHandPress' displayopts=false></resultoptions>\n" +
    "\n" +
    "            <div class='kla-piecharts'>\n" +
    "                <h4 class='text-center kla-pie-header'>Pie Chart Visualizations</h4>\n" +
    "\n" +
    "                <div ng-repeat=\"layout in results.consecHandPress.seriesData.seriesLabels track by $id($index)\" class='kla-pie-container'>\n" +
    "                    <div class='kla-pie-label'>{{layout}}</div>\n" +
    "                    <piechart width=\"500px\" height=\"330px\" source=\"results.consecHandPress\" series=\"$index\"></piechart>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class='tab-pane' id='rowUsage'>\n" +
    "                <h3>Row Usage</h3>\n" +
    "                <p>How often you need to reach farther rows. Ideally when typing, your fingers should not stray too far from home keys. Row usage is not counted in the final score.</p>\n" +
    "            <seriesbarchart width=\"100%\" height=\"350px\" source=\"results.rowUsage\"></seriesbarchart>\n" +
    "            <resulttable source='results.rowUsage'></resulttable>\n" +
    "            <resultoptions source='results.rowUsage'></resultoptions>\n" +
    "\n" +
    "            <div class='kla-piecharts'>\n" +
    "                <h4 class='text-center kla-display-opts-header'>Pie Chart Visualizations</h4>\n" +
    "\n" +
    "                <div ng-repeat=\"layout in results.rowUsage.seriesData.seriesLabels track by $id($index)\" class='kla-pie-container'>\n" +
    "                    <div class='kla-pie-label'>{{layout}}</div>\n" +
    "                    <piechart width=\"500px\" height=\"330px\" source=\"results.rowUsage\" series=\"$index\"></piechart>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class='tab-pane' id='heatMaps'>\n" +
    "            <h3>Heat Maps</h3>\n" +
    "            <p>Visual of where your fingers hit. Red, green, and cyan show more frequent keys; purple and light purple are less frequently hit.</p>\n" +
    "            <div>\n" +
    "                 <div id='heatmap-{{$index}}' class=\"keyboard\" ng-repeat=\"layout in results.layouts\">\n" +
    "                    <keyboardheatmap myindex=\"{{$index}}\" layout=\"layout\" keydata=\"results.keyData[$index]\"></keyboardheatmap>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class='tab-pane' id='words'>\n" +
    "            <div class=\"kla-misc-box\">\n" +
    "                <h3>Words Difficulty</h3>\n" +
    "                <p>Difficulty of words or sequences of letters accounts for 1/16 of the final score.</p>\n" +
    "            </div>\n" +
    "            <div class=\"kla-misc-box\">\n" +
    "                <h3>Words typed with home block</h3>\n" +
    "                <p>Number of words (one or more letters) typed only with index, middle, ring fingers at the nearest home, top, and bottom rows keys, and thumb home keys. Longer words are better. Higher scores are better. Part of the final score.</p>\n" +
    "            </div>\n" +
    "            <seriesbarchart width=\"100%\" height=\"350px\" source=\"results.homeBlockWords\"></seriesbarchart>\n" +
    "            <resulttable source='results.homeBlockWords'></resulttable>\n" +
    "            <resultoptions source='results.homeBlockWords' displayopts=false></resultoptions>\n" +

    "            <div class=\"kla-misc-box\">\n" +
    "                <h3>Words typed with single hand</h3>\n" +
    "                <p>Number of consecutive letters (three or more) all typed by the same hand. Longer sequences are worse. Lower scores are better. Part of the final score.</p>\n" +
    "            </div>\n" +
    "            <seriesbarchart width=\"100%\" height=\"350px\" source=\"results.singleHandWords\"></seriesbarchart>\n" +
    "            <resulttable source='results.singleHandWords'></resulttable>\n" +
    "            <resultoptions source='results.singleHandWords' displayopts=false></resultoptions>\n" +

    "            <div class=\"kla-misc-box\">\n" +
    "                <h3>Words typed with home keys</h3>\n" +
    "                <p>Number of words (one or more letters) typed only with home keys. Longer words are better. Higher scores are better. Not part of final score.</p>\n" +
    "            </div>\n" +
    "            <seriesbarchart width=\"100%\" height=\"350px\" source=\"results.homeKeyWords\"></seriesbarchart>\n" +
    "            <resulttable source='results.homeKeyWords'></resulttable>\n" +
    "            <resultoptions source='results.homeKeyWords' displayopts=false></resultoptions>\n" +

    "        </div>\n" +
    "        <div class='tab-pane' id='miscellaneous'>\n" +

    "            	<div id='freqtableparent'>\n" +

    "					<div>\n" +
    "						<div class=\"kla-misc-box\">\n" +
    "							<h3>Character Frequency</h3>\n" +
    "							<p>Frequency of characters in the supplied corpus text.<br>Lowercase and uppercase are counted separately.<br>Whitespace are denoted with appropriate symbols.</p>\n" +
    "						</div>\n" +
    "						<div class=\"freqtablebox\">\n" +
    "							<table class=\"table table-striped\" >\n" +
    "								<thead>\n" +
    "									<tr>\n" +
	  "                           <th>Rank</th>\n" +
    "										<th>Character</th>\n" +
    "										<th>Appearances</th>\n" +
    " 									<th>Percent</th>\n" +
    "									</tr>\n" +
    "                    </thead>\n" +
    "                    <tbody>\n" +
    "                        <tr ng-repeat=\"char in results.charFreqData\">\n" +
	  "                           <td>#{{$index+1}}</td>\n" +
    "                            <td>{{char.character}}</td>\n" +
    "                            <td>{{char.count}}</td>\n" +
    "                            <td>{{(char.percent * 100).toFixed(2)}}</td>\n" +
    "                        </tr>\n" +
    "                    </tbody>\n" +
    "							</table>\n" +
    "						</div>\n" +
    "					</div>\n" +

    "					<div>\n" +
    "						<div class=\"kla-misc-box\">\n" +
    "							<h3>Bigram Frequency</h3>\n" +
    "							<p>Frequency of bigrams in the supplied corpus text.<br>Lowercase and uppercase are counted separately.<br>Whitespace are denoted with appropriate symbols.</p>\n" +
    "						</div>\n" +
    "						<div class=\"freqtablebox\">\n" +
    "							<table class=\"table table-striped\" >\n" +
    "								<thead>\n" +
    "									<tr>\n" +
	  "                           <th>Rank</th>\n" +
    "										<th>Bigram</th>\n" +
    "										<th>Appearances</th>\n" +
    " 									<th>Percent</th>\n" +
    "									</tr>\n" +
    "                    </thead>\n" +
    "                    <tbody>\n" +
    "                        <tr ng-repeat=\"bigram in results.bigramFreqData\">\n" +
	  "                           <td>#{{$index+1}}</td>\n" +
    "                            <td>{{bigram.bigram}}</td>\n" +
    "                            <td>{{bigram.count}}</td>\n" +
    "                            <td>{{(bigram.percent * 100).toFixed(2)}}</td>\n" +
    "                        </tr>\n" +
    "                    </tbody>\n" +
    "							</table>\n" +
    "						</div>\n" +
    "					</div>\n" +

	  "            <div>\n" +
	  "            	<div class=\"kla-misc-box\">\n" +
	  "            		<h3>Key Frequency</h3>\n" +
	  "            		<p>Frequency of keys hit per layout.</p>\n" +
	  "            	</div>\n" +
	  "           		<div class='text-center'>\n" +
	  "                	<paginate start=\"1\" stop=\"{{results.layouts.length}}\" handler=\"switchHeatmap\"></paginate>\n" +
	  "            			<h4>{{results.layouts[currentHeatmap].keySet.label}}</h4>\n" +
	  "            			<div class=\"freqtablebox\">\n" +
	  "                		<table class=\"table table-striped\" >\n" +
	  "                    		<thead>\n" +
	  "                        		<tr>\n" +
	  "                            	<th>Rank</th>\n" +
	  "                            	<th>Key</th>\n" +
	  "                            	<th>Presses</th>\n" +
	  "                        		</tr>\n" +
	  "                    		</thead>\n" +
	  "                    		<tbody>\n" +
	  "                        		<tr ng-repeat=\"key in results.keyData[currentHeatmap]  | orderBy:['-count', 'primary']\">\n" +
	  "                            	<td>\n" +
	  "                                #{{$index+1}}\n" +
	  "                            	</td>\n" +
	  "                            	<td>\n" +
	  "                                {{getKeyLabel(key.primary, key.shift, key.altGr, key.shiftAltGr)}}\n" +
	  "                            	</td>\n" +
	  "                            	<td>\n" +
	  "                                {{key.count}}\n" +
	  "                            	</td>\n" +
	  "                      	  	</tr>\n" +
	  "                    		</tbody>\n" +
	  "                		</table>\n" +
	  "            			</div>\n" +
	  "            		</div>\n" +
    "            	</div>\n" +
    "            	</div>\n" +
    "            <div class=\"kla-misc-box\">\n" +
    "                <h3>Modifier Key Use</h3>\n" +
    "                <p>How often the Shift, AltGr, Shift+AltGr, and Numpad modifiers are used with characters in the text. Excessive modifiers usage can lead to higher finger usage, thus worse score.</p>\n" +
    "            </div>\n" +
    "            <seriesbarchart width=\"100%\" height=\"350px\" source=\"results.modifierUse\"></seriesbarchart>\n" +
    "            <resulttable source='results.modifierUse'></resulttable>\n" +
    "            <resultoptions source='results.modifierUse' displayopts=false></resultoptions>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    /*
    "    <div class='text-center try-again-btn'>\n" +
    "        <button class=\"btn btn-large\" type=\"button\" ng-click=\"returnToInput()\">Try Another Input</button>\n" +
    "    </div>\n" +
    //*/
    "\n" +
    "</div>"
  );

}]);
